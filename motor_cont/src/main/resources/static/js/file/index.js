layui.use(['jquery', 'element', 'layer', 'form', 'configs', 'template', 'cookie', 'util'], function () {
    var $ = layui.jquery,
        element = layui.element,
        layer = layui.layer,
        form = layui.form,
        template = layui.template,
        config = layui.configs,
        cookie = layui.cookie,
        util = layui.util;
    var areaId;
    var msgCount = 0;
    var tableTpl = $("#motor-status-tpl").html();
    var imgURL = '../../images/';
    var mostateArr = [],
        motorsNUmber = 0,
        lineState = 0; //产线状态: 0 开线, 1 保养;
    // getArea();
    // getItemsInfo();
    //区域
    function getArea() {
        $.ajax({
            url: config.motorBaseUrl + "/area_info/select/v1.0",
            type: "post",
            datatype: "json",
            data: {},
            success: function (result) {
                if (result.stateCode == 0) {
                    $("#motorAreaMenu").html(template("motor-areas-tpl", result));
                    form.render('select');
                    areaId = $("#motorAreaMenu option:checked").val();
                    // getItemsInfo(areaId);
                } else {
                    layer.alert(result.stateMsg);
                }
            }
        })
    }

    //监听区域选择
    // form.on('select(motorAreaMenu)', function (data) {
    //     if(data.value != areaId){
    //         areaId = data.value;
    //         $(".motor-status-table").empty();
    //         msgCount = 0;
    //     }
    //     //clearInterval(timer);
    //     //getItemsInfo(areaId);
    // });

    //加载设备信息
    function getItemsInfo(areaId) {
        $.ajax({
            url: "./js/mows.json",
            type: "post",
            data: {},
            dataType: "json",
            success: function (result) {
                var allDatas = result;
                var nowTime = allDatas.nowTime == undefined ? "" : allDatas.nowTime;
                if (allDatas.data != null && allDatas.data.length > 0) {
                    $(".updTime").text(nowTime + " >> " + util.toDateString(new Date(), 'yyyy-MM-dd HH:mm:ss'));
                    var alertInfoArr = [];
                    var alertLight = "";
                    for (var i = 0; i < allDatas.data.length; i++) {
                        alertLight = allDatas.data[i].areaLight == undefined ? "gray" : allDatas.data[i].areaLight;
                        alertInfoArr.push({
                            areaId: allDatas.data[i].areaId,
                            areaName: allDatas.data[i].areaName,
                            alertLight: alertLight
                        });
                    }
                    var datas = dataFilter(105, allDatas.data);
                    $("span.areaName").text(datas[0].areaName);
                    if (msgCount == 0) {
                        bindAlert(alertInfoArr);
                        bindHtml(datas);
                    } else {
                        changeAlert(alertInfoArr);
                        changeStateLights(datas);
                    }
                }
            }
        });
    }

    //数据过滤，得到所选区域数据
    function dataFilter(areaId, allDatas) {
        var datas = allDatas.filter(function (item) {
            if (item.areaId == areaId) {
                return item;
            }
        });
        return datas;
    }

    var borderObj = {"border-right-color": "transparent", "border-right-width": "2px"};

    //表格拼接
    function bindHtml(data) {
        $(".motor-status-table").html(tableTpl);
        // $(".monitor-status th").css(borderObj);
        $(".motor-status th").css(borderObj);
        $(".mo-troughImg th").css(borderObj);
        var motorLengthArr = [];
        if (!(!list && typeof list != "undefined" && list != 0)) {
            lineState = data[0].list[0].motorMods[0].moState;
            $(`.alertList > ul li[data-areaid=${areaId}]`).attr("data-mostate", lineState);
            var troughId = null, troughName = null, motors = null, colspan = null, moId = null, moName = null,
                sideType = null, sideSort = null, moLight = null, moMonitors = null, monitorLight = null,
                converterId = null, meterID = null;
            for (let i = 0; i < data.length; i++) {
                var list = data[i].list;
                for (let j = 0; j < list.length; j++) {
                    troughId = list[j].troughId;
                    troughName = list[j].troughName;
                    motors = list[j].motorMods;
                    var zouNumArr = []; //同槽位内走道侧马达序号
                    var weiNumArr = []; //同槽位内维修侧马达序号
                    if (motors.length > 0) {
                        //colspan = motors.length/2;
                        for (let m = 0; m < motors.length; m++) {
                            sideType = motors[m].sideType;
                            sideSort = motors[m].sideSort;
                            //同槽位同侧马达计数
                            if (sideType == 1) {
                                zouNumArr.push(sideSort);
                            } else if (sideType == 2) { //维修
                                weiNumArr.push(sideSort);
                            }
                        }
                        //取同槽位马达序号最大值
                        var zouMax = Math.max.apply(null, zouNumArr);
                        var weiMax = Math.max.apply(null, weiNumArr);
                        colspan = zouMax > weiMax ? zouMax : weiMax;
                        //槽位
                        var trn_th = '<th class="one-text" data-troughid="' + troughId + '"  colspan="' + colspan + '"><span class="layui-badge-dot layui-bg-gray layui-hide" data-troughid="' + troughId + '" data-extype="1"></span> ' + troughName + '</th>';
                        $(".mo-troughName").append(trn_th);
                        $(".mo-troughName.zou th[data-troughid]").attr("data-sidetype", 1);
                        $(".mo-troughName.wei th[data-troughid]").attr("data-sidetype", 2);
                        //槽位占位图
                        var tri_td = '<td colspan="' + colspan + '"></td>';
                        $(".mo-troughImg").append(tri_td);

                        //按马达个数添加空td
                        var emptyTD = '';
                        for (let p = 0; p < colspan; p++) {
                            emptyTD += '<td data-idx="' + (p + 1) + '" data-areaid="' + areaId + '" data-troughid="' + troughId + '" data-troughname="' + troughName + '"></td>';
                        }
                        // $(".monitor-status").append(emptyTD);
                        $(".motor-status").append(emptyTD);
                        $(".walkway-motor td").attr("data-sidetype", 1);  //1走道 2维修
                        // $(".walkway-monitor td").attr("data-sidetype",1);
                        $(".maintain-motor td").attr("data-sidetype", 2);
                        // $(".maintain-monitor td").attr("data-sidetype",2);
                        // $(".monitor-status td[data-troughid="+troughId+"][data-idx="+colspan+"]").css(borderObj);
                        $(".motor-status td[data-troughid=" + troughId + "][data-idx=" + colspan + "]").css(borderObj);
                        //再次循环马达，添加状态灯至相应的单元格
                        for (let m = 0; m < motors.length; m++) {
                            moId = motors[m].moId;
                            moName = motors[m].moName;
                            sideType = motors[m].sideType;
                            sideSort = motors[m].sideSort;
                            moMonitors = motors[m].moMonitors;
                            moState = motors[m].moState;  // 0.离线, 1.正常，2.异常，3.维修中
                            motorLengthArr.push(motors.length);
                            if(moState == 3){
                                mostateArr.push(moId);
                            }
                            var isShowDot = false;
                            if (moMonitors == undefined) {
                                monitorLight = "gray";
                                moLight = "gray";
                                converterId = null;
                                meterID = null;
                                isShowDot = true;
                            } else {
                                if (moMonitors.monitorLight == undefined) {
                                    monitorLight = "gray";
                                    isShowDot = true;
                                } else if (moMonitors.monitorLight == "green") {
                                    monitorLight = "green";
                                    isShowDot = false;
                                } else {
                                    monitorLight = moMonitors.monitorLight;
                                    isShowDot = true;
                                }
                                //monitorLight = moMonitors.monitorLight == undefined?'gray':moMonitors.monitorLight;
                                moLight = motors[m].moLight == undefined ? 'gray' : motors[m].moLight;
                                converterId = moMonitors.converterId == undefined ? null : moMonitors.converterId;
                                meterID = moMonitors.meterID == undefined ? null : moMonitors.meterID;
                            }
                            var span = '<span class="one-text">' + moName + '</span>';
                            var motor_img = '';
                            var monitor_img = '';
                            var moClass = stateClass(moLight);
                            var monitorClass = stateClass(monitorLight);
                            //插入马达状态灯
                            motor_img = '<img src="' + imgURL + moLight + '-alarm.gif" class="' + moClass + '" data-monitorlight="' + monitorLight + '" data-isshowdot="' + isShowDot + '" data-extype="2" data-meterid="' + meterID + '" data-converterid="' + converterId + '" data-molight="' + moLight + '" data-troughid="' + troughId + '" data-moid="' + moId + '" data-moname="' + moName + '" data-sidetype="' + sideType + '" data-sidesort="' + sideSort + '">';
                            //保养开关
                            var label = '';
                            if(moState === 3){
                                label = `<label class="switch"><input type="checkbox" data-moid="${moId}" data-mostate="${moState}" checked><div class="slider round"></div><span><em class="yes">保养</em><em class="not">开线</em></span></label>`;
                            }else{
                                label = `<label class="switch"><input type="checkbox" data-moid="${moId}" data-mostate="${moState}"><div class="slider round"></div><span><em class="yes">保养</em><em class="not">开线</em></span></label>`;
                            }
                            var that_motor_td = $('.motor-status-table .motor-status td[data-idx=' + sideSort + '][data-troughid=' + troughId + '][data-sidetype=' + sideType + ']');
                            that_motor_td.append(span + motor_img + label);
                            //插入监测器状态灯
                            // monitor_img = '<img src="' + imgURL+monitorLight+'-alarm.gif" class="'+monitorClass+'" data-extype="1" data-converterid="'+converterId+'" data-monitorlight="'+monitorLight+'" data-troughid="'+troughId+'" data-moid="'+moId+'" data-moname="'+moName+'" data-sidetype="'+sideType+'" data-sidesort="'+sideSort+'">';
                            // var that_monitor_td = $('.motor-status-table .monitor-status td[data-idx='+sideSort+'][data-troughid='+troughId+'][data-sidetype='+sideType+']');
                            // that_monitor_td.append(monitor_img);
                        }
                    }
                    monitorLightsShow(troughId);
                }
            }
            motorsNUmber = motorLengthArr.length;
            if(mostateArr.length === motorLengthArr.length){
                $(`.alertList > ul li[data-areaid=${areaId}] input`).prop("checked",true)
            }else{
                $(`.alertList > ul li[data-areaid=${areaId}] input`).prop("checked",false)
            }
        }
        $(".mo-troughName th").css(borderObj);
        $(".mo-troughImg td").css(borderObj);
    }

    // 点击马达保养开关
    $(".main-table > table").on("change", "td input", function (event) {
        const id = parseInt($(this).attr("data-moid"));
        const isCheck = $(this).is(':checked');
        let mostate = isCheck ? 3 : 1 ; // 1 开线 3 保养
        $.ajax({
            url: config.motorBaseUrl + "/mo_motors/maintain/v1.0",
            type: "get",
            datatype: "json",
            data: {
                moId: id,
                moState: mostate
            },
            success: function (result) {
                if (result.stateCode == 0) {
                    console.log(result.stateMsg);
                    if(isCheck){
                        mostateArr.push(id);
                        if(mostateArr.length === motorsNUmber){
                            $(`.alertList > ul li[data-areaid=${areaId}] input`).prop("checked", true);
                        }else{
                            $(`.alertList > ul li[data-areaid=${areaId}] input`).prop("checked", false);
                        }
                    }else{
                        // 关闭保养，删除对应马达id
                        arrayRemoveItem(mostateArr,id);
                        $(`.alertList > ul li[data-areaid=${areaId}] input`).prop("checked", false);
                    }
                } else {
                    console.log(result.stateMsg);
                }
            }
        });
        event.stopPropagation();
    });
    function arrayRemoveItem(arr, delVal) {
        if (arr instanceof Array) {
            var index = arr.indexOf(delVal);
            if (index > -1) {
                arr.splice(index, 1);
            }
        }
    }
    //websocket改变状态灯
    function changeStateLights(data) {
        if (!(!list && typeof list != "undefined" && list != 0)) {
            var troughId = null, troughName = null, motors = null, colspan = null, moId = null, moName = null,
                sideType = null, sideSort = null, moLight = null, moMonitors = null, monitorLight = null,
                meterID = null;
            for (let i = 0; i < data.length; i++) {
                var list = data[i].list;
                for (let j = 0; j < list.length; j++) {
                    troughId = list[j].troughId;
                    troughName = list[j].troughName;
                    motors = list[j].motorMods;
                    if (motors.length > 0) {
                        //修改状态灯
                        for (let m = 0; m < motors.length; m++) {
                            moId = motors[m].moId;
                            moName = motors[m].moName;
                            sideType = motors[m].sideType;
                            sideSort = motors[m].sideSort;
                            moMonitors = motors[m].moMonitors;
                            var isShowDot = false;
                            if (moMonitors == undefined) {
                                monitorLight = "gray";
                                moLight = "gray";
                                meterID = null;
                                isShowDot = true;
                            } else {
                                if (moMonitors.monitorLight == undefined) {
                                    monitorLight = "gray";
                                    isShowDot = true;
                                } else if (moMonitors.monitorLight == "green") {
                                    monitorLight = "green";
                                    isShowDot = false;
                                } else {
                                    monitorLight = moMonitors.monitorLight;
                                    isShowDot = true;
                                }
                                //monitorLight = moMonitors.monitorLight == undefined?'gray':moMonitors.monitorLight;
                                moLight = motors[m].moLight == undefined ? 'gray' : motors[m].moLight;
                                meterID = moMonitors.meterID == undefined ? null : moMonitors.meterID;
                            }
                            var preMotor = $('.motor-status img[data-sidesort=' + sideSort + '][data-troughid=' + troughId + '][data-sidetype=' + sideType + ']');
                            //var preMonitor = $('.monitor-status img[data-sidesort='+sideSort+'][data-troughid='+troughId+'][data-sidetype='+sideType+']');

                            if (moLight != preMotor.attr("data-molight")) {
                                preMotor.attr("data-molight", moLight);
                                preMotor.attr("src", imgURL + moLight + "-alarm.gif");
                                preMotor.removeClass().addClass(stateClass(moLight));
                            }
                            preMotor.attr("data-isshowdot", isShowDot);
                            preMotor.attr("data-meterid", meterID);
                            // if(monitorLight != preMonitor.attr("data-monitorlight")){
                            //     preMonitor.attr("data-monitorlight",monitorLight);
                            //     preMonitor.attr("src",imgURL+monitorLight+"-alarm.gif");
                            //     preMonitor.removeClass().addClass(stateClass(monitorLight));
                            // }
                        }
                    }
                    monitorLightsShow(troughId);
                }
            }

        }
    }

    //监测器状态灯
    function monitorLightsShow(troughId) {
        var zouMonitorlight = $('.walkway-motor img[data-troughid="' + troughId + '"][data-isshowdot="true"]');
        var weiMonitorlight = $('.maintain-motor img[data-troughid="' + troughId + '"][data-isshowdot="true"]');
        var zouSpan = $('.mo-troughName.zou th[data-troughid="' + troughId + '"] span.layui-badge-dot');
        var weiSpan = $('.mo-troughName.wei th[data-troughid="' + troughId + '"] span.layui-badge-dot');
        var zouColorArr = [], weiColorArr = [];
        var this_idx_zou = 0, this_meterID_zou = null, this_idx_wei = 0, this_meterID_wei = null;
        if (zouMonitorlight.length > 0) {
            zouSpan.removeClass("layui-hide");
            $(zouMonitorlight).each(function (index, element) {
                zouColorArr.push($(element).attr("data-monitorlight"));
            });
            if (zouColorArr.indexOf("red") != -1) {
                this_idx_zou = zouColorArr.indexOf("red") + 1;
                zouSpan.removeClass("toExcep noMonitor").addClass("toExcep");
                this_meterID_zou = $('.walkway-motor td[data-idx="' + this_idx_zou + '"]').find("img").attr("data-meterid");
                zouSpan.attr("data-meterid", this_meterID_zou);
            } else {
                if (zouColorArr.indexOf("blue") != -1) {
                    this_idx_zou = zouColorArr.indexOf("red") + 1;
                    zouSpan.removeClass("toExcep noMonitor").addClass("toExcep");
                    this_meterID_zou = $('.walkway-motor td[data-idx="' + this_idx_zou + '"]').find("img").attr("data-meterid");
                    zouSpan.attr("data-meterid", this_meterID_zou);
                } else {
                    if (zouColorArr.indexOf("gray") != -1) {
                        zouSpan.removeClass("toExcep noMonitor").addClass("noMonitor");
                    }
                }
            }
        } else {
            zouSpan.addClass("layui-hide");
        }
        if (weiMonitorlight.length > 0) {
            weiSpan.removeClass("layui-hide");
            $(weiMonitorlight).each(function (index, element) {
                weiColorArr.push($(element).attr("data-monitorlight"));
            });
            if (weiColorArr.indexOf("red") != -1) {
                this_idx_wei = weiColorArr.indexOf("red") + 1;
                weiSpan.removeClass("toExcep noMonitor").addClass("toExcep");
                this_meterID_wei = $('.maintain-motor td[data-idx="' + this_idx_wei + '"]').find("img").attr("data-meterid");
                weiSpan.attr("data-meterid", this_meterID_wei);
            } else {
                if (weiColorArr.indexOf("blue") != -1) {
                    this_idx_wei = weiColorArr.indexOf("red") + 1;
                    weiSpan.removeClass("toExcep noMonitor").addClass("toExcep");
                    this_meterID_wei = $('.maintain-motor td[data-idx="' + this_idx_wei + '"]').find("img").attr("data-meterid");
                    weiSpan.attr("data-meterid", this_meterID_wei);
                } else {
                    if (weiColorArr.indexOf("gray") != -1) {
                        weiSpan.removeClass("toExcep noMonitor").addClass("noMonitor");
                    }
                }
            }
        } else {
            weiSpan.addClass("layui-hide");
        }
    }

    //插入报警面板
    function bindAlert(alertInfoArr) {
        $(".alertList ul").empty();
        var li = '' ;
        for (var i = 0; i < alertInfoArr.length; i++) {
            var _areaId = alertInfoArr[i].areaId,areaName = alertInfoArr[i].areaName,color = alertInfoArr[i].alertLight;
            li += `<li data-areaid="${_areaId}"><div><img src="${imgURL}${color}-alarm.gif"><span>${areaName}</span></div>
                    <label class="switch line fr"><input type="checkbox"><div class="slider round"></div><span><em class="yes">保养</em><em class="not">开线</em></span></label></li>`
        }
        $(".alertList ul").append(li);
        $(".alertList ul li[data-areaid=" + areaId + "]").addClass("current");
    }

    function changeAlert(alertInfoArr) {
        for (var i = 0; i < alertInfoArr.length; i++) {
            $(".alertList ul li[data-areaid=" + alertInfoArr[i].areaId + "]").find("img").attr("src", imgURL + alertInfoArr[i].alertLight + "-alarm.gif");
        }
    }

    // 点击产线名切换产线
    $(".alertList > ul").on("click", "li > div", function () {
        areaId = $(this).parent("li").attr("data-areaid");
        $(".motor-status-table").empty();
        msgCount = 2;
    });
    // 点击产线保养开关
    $(".alertList > ul").on("change", "li input", function (event) {
        const id = $(this).parents("li").attr("data-areaid");
        const isCheck = $(this).is(':checked');
        const moState = isCheck ? 3 : 1 ;
        $.ajax({
            url: config.motorBaseUrl + "/area/areaMaintain/v1.0",
            type: "get",
            datatype: "json",
            data: {
                areaId: id,
                moState: moState
            },
            success: function (result) {
                if (result.stateCode == 0) {
                    console.log(result.stateMsg);
                    if(isCheck){
                        $(".main-table > table td input").prop("checked", true);
                    }else{
                        $(".main-table > table td input").prop("checked", false);
                    }
                } else {
                    console.log(result.stateMsg);
                }
            }
        });
        event.stopPropagation();
    });

    //状态灯跳转：红蓝灯 toReal 到异常查询，绿黄灯 toExcep 到波形图
    function stateClass(lights) {
        if (lights == "red" || lights == "blue") {
            return "toExcep"
        } else if (lights == "green" || lights == "yellow" || lights == "gray") {
            return "toReal"
        } else {
            return ''
        }
    }

    var iframe = window.parent.document.getElementsByTagName('iframe')[0];
    var menulist = JSON.parse(sessionStorage.getItem("menulist"));
    if(menulist == undefined){
        getMenuData();
    }
    $("table.motor-status-table").on("click", ".motor-status img", function () {
        var this_motor = {
            "areaId": areaId,
            "troughId": $(this).attr("data-troughid"),
            "moId": $(this).attr("data-moid"),
            "moName": $(this).attr("data-moname"),
            "exType": $(this).attr("data-extype"),
            "converterId": $(this).attr("data-converterid")
        };
        $.cookie("this_motor", JSON.stringify(this_motor));
        sessionStorage.setItem("this_motor", JSON.stringify(this_motor));
        var menu = [];
        if ($(this).hasClass("toExcep")) {
            // window.location.href = "../report/exception-rec.html";
            menu = menulist.filter(item => item.menuUrl === './pages/report/exception-rec.html')
        } else if ($(this).hasClass("toReal")) {
            // window.location.href = "realtime.html";
            menu = menulist.filter(item => item.menuUrl === './pages/device/realtime.html')
        };
        $(iframe).parents('div.page-content').siblings(".left-nav").find(`li#menu${menu[0].menuId}`).click();
    });

    function getMenuData() {
        $.ajax({
            url: config.motorBaseUrl + "/sys/getMeun/v1.0",
            type: "post",
            datatype: "json",
            data: {},
            success: function (result) {
                if (result.stateCode == 0) {
                    menulist = result.data;
                } else {
                    console.log(result.stateMsg);
                }

            }
        })
    }

    //监测器跳转
    /*$("table.motor-status-table").on("click", "span.layui-bg-gray", function () {
        if ($(this).hasClass("noMonitor")) {
            layer.alert("未安装监测器");
        } else {
            var this_motor = {
                "areaId": areaId,
                "troughId": $(this).attr("data-troughid"),
                "moId": 0,
                "moName": "",
                "exType": $(this).attr("data-extype"),
                "meterID": $(this).attr("data-meterid")
                //"converterId" : $(this).attr("data-converterid")
            };
            $.cookie("this_motor", JSON.stringify(this_motor));
            window.location.href = "../report/exception-rec.html";
        }
    });*/

    //槽位名宽度
    function troughNameWidth(colspan) {
        // var w = 58 * colspan;
        var w = $('tr.motor-status td').width() * colspan;
        var width = 'width:' + w + 'px;max-width:' + w + 'px';
        return width;
    }

    //WebSocket
    var socket;
    var countdownTimer = null;
    initSocket();

    //登录过后初始化socket连接
    function initSocket() {
        if (typeof (WebSocket) == "undefined") {
            console.log("您的浏览器不支持WebSocket");
        } else {
            console.log("您的浏览器支持WebSocket/websocket");
        }
        //socket连接地址: 注意是ws协议
        socket = new WebSocket(config.motorWSBaseUrl);
        socket.onopen = function () {
            console.log("Socket 已打开");
            if (msgCount == 0) {
                $(".main-table").after('<div class="loading-wrap"><div class="countdown"></div><div id="seconds" class="second"></div></div>');
                initSec();
                // $(".motor-status-table").append('<i class="layui-icon layui-anim layui-icon-loading layui-anim-rotate layui-anim-loop"></i>');
            }
        };
        //获得消息事件
        socket.onmessage = mo_wsMsg;
        //关闭事件
        socket.onclose = function () {
            console.log("Socket已关闭");
        };
        //错误事件
        socket.onerror = function () {
            alert("Socket发生了错误");
        };
        $(window).on("unload", function () {
            socket.close();
        });
    }

    //点击按钮发送消息
    // function send() {
    //     console.log("发送消息: " + $("#msg").val());
    //     socket.send($("#msg").val());
    // }

    //loading
    function initSec(){
        var second = document.getElementById("seconds");
        var num = 5;
        function count(){
            second.innerHTML = num;
            num--;
            if(num <= 0){
                num = 5;
            }
        }
        count();
        countdownTimer = setInterval(count, 1000);
    }

    //接收消息msg
    function mo_wsMsg(msg) {
        var allDatas = JSON.parse(msg.data);
        var nowTime = allDatas.nowTime == undefined ? "" : allDatas.nowTime;
        if (allDatas.data != null && allDatas.data.length > 0) {
            $(".updTime").text(nowTime + " >> " + util.toDateString(new Date(), 'yyyy-MM-dd HH:mm:ss'));
            var alertInfoArr = [];
            var alertLight = "";
            for (var i = 0; i < allDatas.data.length; i++) {
                alertLight = allDatas.data[i].areaLight == undefined ? "gray" : allDatas.data[i].areaLight;
                alertInfoArr.push({
                    areaId: allDatas.data[i].areaId,
                    areaName: allDatas.data[i].areaName,
                    alertLight: alertLight
                });
            }
            if (msgCount == 0){
                areaId = alertInfoArr[0].areaId ;
                clearInterval(countdownTimer);
                $(".loading-wrap").remove();
                initTable(areaId,allDatas,alertInfoArr);
            }else if (msgCount == 2) {
                initTable(areaId,allDatas,alertInfoArr);
            } else {
                var datas = dataFilter(areaId, allDatas.data);
                changeAlert(alertInfoArr);
                changeStateLights(datas);
            }
        }
        msgCount = 1;
        //跑马灯
        // var alertMsg = allDatas.alert;
        // if(alertMsg != null && alertMsg != undefined && alertMsg != ''){
        //     var alertMsgArr = alertMsg.split(",");
        //     var lis = '';
        //     for (var i = 0;i<alertMsgArr.length;i++){
        //         lis += '<li>'+alertMsgArr[i]+'</li>'
        //     }
        //     $('.marquee ul').empty();
        //     $('.marquee ul').append(lis);
        // }
    }
    function initTable(areaId,allDatas,alertInfoArr){
        var datas = dataFilter(areaId, allDatas.data);
        $("span.areaName").text(datas[0].areaName);
        bindAlert(alertInfoArr);
        bindHtml(datas);
    }
});