layui.use(['laydate', 'layer', 'form', 'table', 'configs', 'template','excel','formSelects'], function () {
    var $ = layui.jquery,
        laydate = layui.laydate,
        layer = layui.layer,
        form = layui.form,
        table = layui.table,
        template = layui.template,
        config = layui.configs,
        excel = layui.excel,
        formSelects = layui.formSelects;
    var tableRender = null;
    var areaId = null;
    var today = {
        tableName: 'mo_data_rec',
        tableNameShow: formatDate(new Date()).split(' ')[0],
    };
    initAll();

    function initAll(){
        getTableOption();
        getArea();
        initDate();
        // formSelects.render('select-plc');
        // formSelects.btns('select-plc', []);
        // formSelects.value('select-plc', [1],true); //默认开
    }

    function initDate(startValue){
        var st = new Date(new Date().getTime() - 3600*1000 );
        startValue = startValue || st;
        laydate.render({
            elem: '#startDate',
            value: secondZero(startValue),
            // format: 'yyyy-MM-dd HH:mm:ss',
            type: 'time',
            ready: function(date){
                var hourLi = $('.laydate-time-show .laydate-time-list > li:nth-child(2)');
                hourLi.nextAll().remove();
                $('.laydate-time-show .laydate-time-list > li').css("width", "50%");
            }
        });
        laydate.render({
            elem: '#endDate',
            value: secondZero(new Date()),
            // format: 'yyyy-MM-dd HH:mm:ss',
            type: 'time',
            ready: function(date){
                var hourLi = $('.laydate-time-show .laydate-time-list > li:nth-child(2)');
                hourLi.nextAll().remove();
                $('.laydate-time-show .laydate-time-list > li').css("width", "50%");
            }
        });
    };

    //获取 table-name
    function getTableOption() {
        $.ajax({
            url: config.motorBaseUrl + "/mo_data_rec/select/tabeName/v1.0",
            type: "post",
            datatype: "json",
            data: {},
            success: function (result) {
                if (result.stateCode == 0) {
                    $(".tableMenu").html(template("table-name-tpl", result));
                    var opt = `<option value="${today.tableName}" selected>${today.tableNameShow}</option>`;
                    $(".tableMenu").prepend(opt);
                    form.render('select');
                } else {
                    layer.alert(result.stateMsg);
                }
            }
        })
    }

    //获取区域
    function getArea() {
        $.ajax({
            url: config.motorBaseUrl + "/area_info/select/v1.0",
            type: "post",
            datatype: "json",
            data: {},
            success: function (result) {
                if (result.stateCode == 0) {
                    $(".areaMenu").html(template("motor-areas-tpl", result));
                    form.render('select');
                    //监听区域选择
                    form.on('select(areaMenu)', function(data) {
                        $(".troughMenu").html("");
                        $(".motorMenu").html('<option value="0">全部</option>');
                        areaId = data.value;
                        getTrough(areaId);
                    });
                } else {
                    layer.alert(result.stateMsg);
                }
            }
        })
    }

    //按区域查槽位
    function getTrough(areaId) {
        $.ajax({
            url: config.motorBaseUrl + "/mo_trough/selectAreaTrough/v1.0",
            type: "post",
            datatype: "json",
            data: {
                areaId: areaId
            },
            success: function (result) {
                if (result.stateCode == 0) {
                    $(".troughMenu").html(template("motor-trough-tpl", result));
                    form.render('select');
                    //监听槽位选择
                    form.on('select(troughMenu)', function(data) {
                        $(".motorMenu").html("");
                        var troughId = data.value;
                        getMotors(troughId);
                    });
                } else {
                    layer.alert(result.stateMsg);
                }
            }
        })
    };

    // 按槽位查马达
    function getMotors(troughId) {
        $.ajax({
            url: config.motorBaseUrl + "/mo_motors/selectMortor/v1.0",
            type: "post",
            datatype: "json",
            data: {
                troughId: troughId
            },
            success: function (result) {
                if (result.stateCode == 0) {
                    $(".motorMenu").html(template("motors-tpl", result));
                    form.render('select');
                } else {
                    layer.msg(result.stateMsg);
                }
            }
        })
    }

    //查询
    $(".searchBtn").click(function () {
        // $(".reportTable").append('<i class="layui-icon layui-icon-loading layui-anim-rotate layui-anim-loop"></i>');
        form.on('submit(search)', function(data){
            // 从 tableName 截取日期   "mo_data_rec_20200313000001"
            var date = '';
            if(data.field.tableName === today.tableName){
                date = today.tableNameShow
            }else{
                var dateStr = data.field.tableName.slice(12,20);  // 20200313
                date = dateStr.slice(0,4) + '-' + dateStr.slice(4,6) + '-' + dateStr.slice(6);
            }
            var fieldData = {
                tableName: data.field.tableName,
                areaId:data.field.areaId,
                troughId: data.field.troughId,
                moId: data.field.moId,
                plcState: data.field.plcState,
                startTime: date + ' ' + data.field.startTime,
                endTime: date + ' ' + data.field.endTime,
            };
            // console.log(fieldData);
            getTableData(fieldData);
            return false;
        });
    });

    //获取表格数据
    function getTableData(data) {
        $.ajax({
            url: config.motorBaseUrl + "/mo_data_rec/select/tabeData/v1.0",
            type: "post",
            datatype: "json",
            beforeSend: function () {
                $('.loading').removeClass("layui-hide");
                $('.tables').addClass("layui-hide");
            },
            data: data,
            success: function (result) {
                if (result.stateCode == 0) {
                    renderTable(result.data);
                } else {
                    layer.msg(result.stateMsg);
                }
            },
            complete: function () {
                $('.loading').addClass("layui-hide");
                $('.tables').removeClass("layui-hide");
            }
        })
    }

    //渲染表格
    function renderTable(data) {
        tableRender = table.render({
            elem: '.reportTable'
            , id:"motorRecord"
            , data: data
            , cols: [[
                {field: 'areaName', title: '区域'}
                /*,{field: 'troughName', title: '槽位'}*/
                ,{field: 'moName', title: '马达名称'}
                ,{field: 'strRecTime', title: '记录时间'}
                /*, {field: 'plcState', title: '启停信号',  templet: '<div>{{d.plcState==1?"开":"关"}}</div>'}*/
                , {field: 'shockData', title: '振幅数据'}
                , {field: 'lowerLimit', title: '振幅下限', templet:'<div>{{ d.lowerLimit == -1? "未启用" : d.lowerLimit}}</div>'}
                , {field: 'filg', title: '是否报警'}
                /*
                , {field: 'upperLimitInner', title: '预警上限', templet:'<div>{{ d.upperLimitInner == -1? "未启用" : d.upperLimitInner}}</div>'}
                , {field: 'lowerLimitInner', title: '预警下限', templet:'<div>{{ d.lowerLimitInner == -1? "未启用" : d.lowerLimitInner}}</div>'}*/
            ]]
            // ,overflow: {
            //   type: 'tips'
            //   ,color: 'black' // 字体颜色
            //   ,bgColor: 'white' // 背景色
            // }
            , page: {
                layout: ['prev', 'page', 'next', 'count', 'skip', 'limit'] //自定义分页布局
                , groups: 1 //只显示 5 个连续页码
                , first: false
                , last: false
                , limits: [10, 20, 50, 100, 500]
            }
            , limit: 10
            , loading: true
            , done: function (res) {
                //soulTable.render(this);
            }
        });
    }

    //导出
    $(".exportBtn").on("click",function () {
        excel.exportTable2Excel(tableRender, "马达表记录");
        /* var st = new Date();
         console.log("st="+st);
         soulTable.export(tableRender, {
           filename: '马达振动记录.xlsx', // 文件名
         });
         var et = new Date();
         console.log("et="+et);
         console.log(et - st);*/
    });

    // 时间秒数为00
    function secondZero(date) {
        var date = date || new Date();
        var h = date.getHours();
        h = h < 10 ? '0' + h : h;
        var m = date.getMinutes();
        m = m < 10 ? '0' + m : m;
        return h+':'+m+':00';
    }
});
