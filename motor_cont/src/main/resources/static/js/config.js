layui.define(['jquery','element'], function (exports) {
    var $ = layui.jquery, element = layui.element;
    var host = window.location.host;
    var config = {};
    config = {
        baseUrl: "http://" + host,
        //默认是否展开左侧菜单 0收起  1展开
        isShowLeft: 1,
        //网页<head>中的<title>
        title: "佳运通综合平台",
        motor: "motor",//供电马达
        // 不同项目，实时刷新间隔 （毫秒）
        motorInter: 3000,
        //供电马达设置：isShow 是否显示，text 显示的文字
        motorConf: {
            amplitude: {isShow: true, text: "振幅"}, //振幅
            displacement: {isShow: false, text: "位移"}, //位移
            acceleration: {isShow: false, text: "加速度"}  //加速度
        },
        platformBaseUrl:"",
        motorBaseUrl: "",
        GetQueryString: function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);//search,查询？后面的参数，并匹配正则
            if (r != null) {
                return unescape(r[2]);
            }
            return null;
        },
        tdTitle: function () {
            $('th').each(function (index, element) {
                $(element).attr('title', $(element).text());
            });
            $('td').each(function (index, element) {
                $(element).attr('title', $(element).text());
            });
        }
    };
    config.platformBaseUrl = config.baseUrl + "/platform" ;
    config.motorBaseUrl = config.baseUrl + "/" + config.motor;
    config.motorWSBaseUrl = "ws://" + host + "/" + config.motor + "/mowebsocket";
    exports('configs', config);
});