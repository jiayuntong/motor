layui.config({
  base: '{/}../../../js/'      //自定义layui组件的目录
}).extend({ //设定组件别名
  configs: 'config',
  echarts: 'extends/echarts',
  template: 'extends/template-web',
  cookie: 'extends/jq-cookie',
  formSelects: 'extends/formSelects-v4.min',
  iconPicker: 'extends/iconPicker',
  excel: '{/}../../lib/layui/excel'
}).use(['jquery','element','layer','template','configs'], function(){
  var $ = layui.jquery,
      element = layui.element,
      layer = layui.layer,
      laytpl = layui.laytpl,
      template = layui.template,
      config = layui.configs;
  /*打印*/
  $(".printBtn").click(function(){
    window.print();
  });
});

  /**
   * @param arr : 需要转换的数组
   * @return obj
   */
   function toObj (arr) {
    var obj = {};
    obj.data = arr;
    return obj;
  }

  /** 转换日期格式
   * @param time : 时间戳 (不传默认为系统当前时间)
   * @return string : 返回格式为 '2018-07-05 08:00:00' 的字符串
   */
  function formatDate(time) {
    var date ;
    if(isNaN(parseInt(time))){
      date = new Date(); //形参为非数字,data默认为当前日期
    }else{
      //如果是10位时间戳,则转补全13位
      time = (time + "").length === 10 ? time*1000 : time;
      date = new Date(time);
    }
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? ('0' + m) : m;
    var d = date.getDate();
    d = d < 10 ? ('0' + d) : d;
    var h = date.getHours();
    h = h < 10 ? ('0' + h) : h;
    var minute = date.getMinutes();
    var second = date.getSeconds();
    minute = minute < 10 ? ('0' + minute) : minute;
    second = second < 10 ? ('0' + second) : second;
    return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
  };

  /** 分钟转毫秒
   * @param min :  (不传默认为10分钟)
   * @return number : 返回毫秒数
   */
  function MinToMs(min){
    var ms = 60 * 1000;
    min ? ms *= min : ms *= 10 ;
    return ms;
  }

  // 数组去重
  function unique(arr){
    var res = [];
    var obj = {};
    for(var i=0;i<arr.length;i++){
      if(!obj[arr[i]]){
        res.push(arr[i]);
        obj[arr[i]] = 1;
      }
    }
    return res;
  }

  //重构数组,合并元素相同属性值
  function refactorData(arr){
    var obj1 = {};
    var result = [];
    arr.forEach(function(el){
      var key = el.LabelArea;
      if(typeof obj1[key] === 'undefined'){
        obj1[key] = [];
      }
      obj1[key].push(el);
    })
    var keys = Object.keys(obj1);
    for(var i = 0; i < keys.length; i++){
      result.push({LabelArea:keys[i],items:obj1[keys[i]]});
    }
    return result;
  }




