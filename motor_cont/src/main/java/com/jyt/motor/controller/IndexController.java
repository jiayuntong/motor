package com.jyt.motor.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Slf4j
public class IndexController {

    @RequestMapping("/index")
    public String index() {
        return "index";
    }

    @RequestMapping("/dashboard")
    public String dashboard() {
        return "dashboard/dashboard";
    }

    @RequestMapping("/device/index")
    public String device() {
        return "device/index";
    }

    @RequestMapping("/report/motors-rec")
    public String motorsrec() {
        return "report/motors-rec";
    }

    @RequestMapping("/report/exception-rec")
    public String exceptionrec() {
        return "report/exception-rec";
    }

    @RequestMapping("/report/trend-chart")
    public String chart() {
        return "report/trend-chart";
    }

    @RequestMapping("/report/analysis")
    public String analysis() { return "report/analysis"; }

    @RequestMapping("/settings/motors-setting")
    public String motorset() {
        return "settings/motors-setting";
    }

    @RequestMapping("/settings/maintain-rec")
    public String maintainrec() {
        return "settings/maintain-rec";
    }

    @RequestMapping("/settings/area-setting")
    public String areaset() {
        return "settings/area-setting";
    }

    @RequestMapping("/settings/monitors-setting")
    public String monitorset() {
        return "settings/monitors-setting";
    }

    @RequestMapping("/settings/monitors-converters")
    public String converters() {
        return "settings/monitors-converters";
    }

    @RequestMapping("/system/sys-setting")
    public String sysset() {
        return "system/sys-setting";
    }

    @RequestMapping("/admin/menus")
    public String menus() {
        return "admin/menus";
    }

    @RequestMapping("/admin/users")
    public String users() {
        return "admin/users";
    }

    @RequestMapping("/admin/role")
    public String role() {
        return "admin/role";
    }

    @RequestMapping("/member/password")
    public String psw() {
        return "member/password";
    }
}
