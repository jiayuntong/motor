package com.jyt.motor.controller;


import com.jyt.motor.constant.ComConstant;
import com.jyt.motor.model.SysUsers;
import com.jyt.motor.service.PFLoginsService;
import com.jyt.motor.service.PFUsersService;
import com.jyt.motor.util.SHA256Util;
import com.jyt.motor.vo.ResponseMod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

/**
 * TODO: 这是用户控制器层
 *
 * @version 1.0.0
 * @author: zhangWeiBin
 **/
@RestController()
@RequestMapping("/platform/user")
@Slf4j
public class PFUsersController {

    /**
     * The Login service.
     */
    @Autowired
    PFLoginsService loginService;

    /**
     * The User service.
     */
    @Autowired
    PFUsersService userService;

    /**
     * 用户登陆功能
     *
     * @param sysUsers the sys users
     * @return response mod
     */
    @RequestMapping("/login/v1.0")
    public ResponseMod login(SysUsers sysUsers) {
        ResponseMod mod = new ResponseMod();
        try {
            SysUsers user = userService.getUserName(sysUsers.getUserName());
            if (user == null) {
                mod.setStateCode(ComConstant.FAILCODE);
                mod.setStateMsg("用户名不存在！");
                return mod;
            }
            Map<String, Object> map = loginService.login(sysUsers);
            Boolean flag = (Boolean) map.get("isLogin");
            if (flag) {
                mod.setStateMsg("登录成功！");
            } else {
                mod.setStateMsg("密码错误！");
            }
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setData(map);
        } catch (Exception e) {
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg("数据库连接失败！");
            log.error(e.getMessage(), e);
        }

        return mod;
    }

    /**
     * 获取所有用户功能
     *
     * @return all
     */
    @ResponseBody
    @RequestMapping("/getAll/v1.0")
    public ResponseMod getAll() {
        ResponseMod mod = new ResponseMod();
        try {
            List<SysUsers> users = userService.getAll();
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setData(users);
        } catch (Exception e) {
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg("数据库连接失败！");
            log.error(e.getMessage(), e);
        }

        return mod;
    }

    /**
     * 用户密码修改
     *
     * @param userName    the user name
     * @param oldPassword the old password
     * @param newPwd      the new pwd
     * @param newPwd2     the new pwd 2
     * @return chg pwd
     */
    @ResponseBody
    @RequestMapping("/chg_pwd/v1.0")
    public ResponseMod setChgPwd(String userName, String oldPassword, String newPwd, String newPwd2) {
        ResponseMod mod = new ResponseMod();
        try {
            SysUsers user = userService.getUserName(userName);
            if (user == null) {
                mod.setStateCode(ComConstant.FAILCODE);
                mod.setStateMsg("用户名不存在！");
                return mod;
            }
            if (user.getUserPwd().equals(SHA256Util.getSHA256StrJava(oldPassword))) {//登录密码相同
                if (newPwd.equals(newPwd2)) {//两次密码相同
                    userService.setChgPwd(user.getUserId(), newPwd);
                    mod.setStateCode(ComConstant.SUCCESSCODE);
                    mod.setStateMsg(ComConstant.SUCCESSMSG);
                } else {
                    mod.setStateCode(ComConstant.FAILCODE);
                    mod.setStateMsg("两次密码不相同！");
                }
            } else {
                mod.setStateCode(ComConstant.FAILCODE);
                mod.setStateMsg("密码不正确！");
            }
        } catch (Exception e) {
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
            log.error(e.getMessage(), e);
        }

        return mod;
    }

    /**
     * 用户添加功能
     *
     * @param sysUsers the sys users
     * @return response mod
     */
    @ResponseBody
    @RequestMapping("/userAdd/v1.0")
    public ResponseMod userAdd(SysUsers sysUsers) {
        ResponseMod mod = new ResponseMod();
        try {

            List<SysUsers> users = userService.getAll();
            for (SysUsers use : users) {
                if (use.getUserName().equals(sysUsers.getUserName())) {
                    mod.setStateCode(ComConstant.FAILCODE);
                    mod.setStateMsg("用户名已存在！");
                    return mod;
                }
            }

            userService.userAdd(sysUsers);
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg("新增用户失败！");
            log.error(e.getMessage(), e);
        }

        return mod;
    }

    @ResponseBody
    @RequestMapping("/edit/v1.0")
    public ResponseMod userEdit(SysUsers sysUsers) {
        ResponseMod mod = new ResponseMod();
        try {
            List<SysUsers> users = userService.getAll();
            for (SysUsers use : users) {
                if (use.getUserId() != sysUsers.getUserId() && use.getUserName().equals(sysUsers.getUserName())) {
                    mod.setStateCode(ComConstant.FAILCODE);
                    mod.setStateMsg(ComConstant.FAILMSG);
                    mod.setStateMsg("用户名已存在！");
                    return mod;
                }
            }
            userService.updateSysUsers(sysUsers);
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg("新增用户失败！");
            log.error(e.getMessage(), e);
        }

        return mod;
    }

    @ResponseBody
    @RequestMapping("/delete/v1.0")
    public ResponseMod userDel(SysUsers sysUsers) {
        ResponseMod mod = new ResponseMod();
        try {
            userService.delSysUser(sysUsers);
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg("新增用户失败！");
            log.error(e.getMessage(), e);
        }
        return mod;
    }
}
