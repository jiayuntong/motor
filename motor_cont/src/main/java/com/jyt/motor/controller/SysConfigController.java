package com.jyt.motor.controller;

import com.jyt.motor.constant.ComConstant;
import com.jyt.motor.dao.SysConfigMapper;
import com.jyt.motor.model.SysConfig;
import com.jyt.motor.service.ReceiveEBTService;
import com.jyt.motor.service.ReceiveIWIRService;
import com.jyt.motor.util.SqlServer;
import com.jyt.motor.vo.ResponseMod;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.*;

@RestController
@RequestMapping("/motor")
@Slf4j
public class SysConfigController {
    @Autowired
    SysConfigMapper sysConfigMapper;

    @Autowired
    ReceiveEBTService receiveEBTService;

    @Autowired
    ReceiveIWIRService receiveIWIRService;




    /**
     * 获取所有配置项
     *
     * @return meun
     */
    @RequestMapping("/config/configAll/v2.0")
    public ResponseMod getConfigAll() {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(sysConfigMapper.selectAll());
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("获取配置异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 新增配置
     *
     * @return meun
     */
    @RequestMapping("/config/insert/v2.0")
    public ResponseMod ConfigInsert(SysConfig sysConfig) {
        ResponseMod mod = new ResponseMod();
        try {
            if (StringUtils.isEmpty(sysConfig.getConfCategory())
                    || StringUtils.isEmpty(sysConfig.getConfKey())
                    || StringUtils.isEmpty(sysConfig.getConfValue())
                    || StringUtils.isBlank(sysConfig.getConfCategory())
                    || StringUtils.isBlank(sysConfig.getConfKey())
            ) {
                mod.setData("存在空白项");
                mod.setStateCode(ComConstant.FAILCODE);
                mod.setStateMsg(ComConstant.FAILMSG);
                return mod;
            }
            mod.setData(sysConfigMapper.insertSelective(sysConfig));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("新增配置异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }


    /**
     * 修改配置
     *
     * @return meun
     */
    @RequestMapping("/config/upConfig/v2.0")
    public ResponseMod ConfigUpdate(SysConfig sysConfig) {
        ResponseMod mod = new ResponseMod();
        try {
            if (StringUtils.isEmpty(sysConfig.getConfCategory())
                    || StringUtils.isEmpty(sysConfig.getConfKey())
                    || StringUtils.isEmpty(sysConfig.getConfValue())
                    || StringUtils.isBlank(sysConfig.getConfCategory())
                    || StringUtils.isBlank(sysConfig.getConfKey())
            ) {
                mod.setData("存在空白项");
                mod.setStateCode(ComConstant.FAILCODE);
                mod.setStateMsg(ComConstant.FAILMSG);
                return mod;
            }
            if (sysConfig.getId() == null
                    || sysConfig.getId() <= 0) {
                mod.setData("非法请求");
                mod.setStateCode(ComConstant.FAILCODE);
                mod.setStateMsg(ComConstant.FAILMSG);
                return mod;
            }
            mod.setData(sysConfigMapper.updateByPrimaryKeySelective(sysConfig));
            //更新缓存
            if("motor_exception_times".equals(sysConfig.getConfKey())||"motor_offline_times".equals(sysConfig.getConfKey())||"motor_recovery_times".equals(sysConfig.getConfKey()))
            {
                receiveEBTService.clearConf();
                receiveIWIRService.clearConf();
            }else if("get_data_ebt_cron".equals(sysConfig.getConfKey()))
            {
                receiveEBTService.clearGetDataCron();
            }else if("save_data_ebt_cron".equals(sysConfig.getConfKey()))
            {
                receiveEBTService.clearSaveDataCron();
            }else if("get_data_iwir_cron".equals(sysConfig.getConfKey()))
            {
                receiveIWIRService.clearGetDataCron();
            }else if("save_data_iwir_cron".equals(sysConfig.getConfKey()))
            {
                receiveIWIRService.clearSaveDataCron();
            }else if("abnormal_SMS_mobiles".equals(sysConfig.getConfKey()))
            {
                receiveEBTService.clearMobiles();
                receiveIWIRService.clearMobiles();
            }



            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("修改配置异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 删除配置
     *
     * @return meun
     */
    @RequestMapping("/config/deleteById/v2.0")
    public ResponseMod ConfigDelete(SysConfig sysConfig) {
        ResponseMod mod = new ResponseMod();
        try {
            if (StringUtils.isEmpty(sysConfig.getConfKey())
                    || StringUtils.isBlank(sysConfig.getConfKey())
                    || sysConfig.getId() == null
                    || sysConfig.getId() <= 0
            ) {
                mod.setData("非法请求");
                mod.setStateCode(ComConstant.FAILCODE);
                mod.setStateMsg(ComConstant.FAILMSG);
                return mod;
            }
            mod.setData(sysConfigMapper.deleteByPrimaryKey(sysConfig.getId()));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("删除配置异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }
}
