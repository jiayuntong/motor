package com.jyt.motor.controller;

import com.jyt.motor.constant.ComConstant;
import com.jyt.motor.model.SysMenus;
import com.jyt.motor.model.SysRoles;
import com.jyt.motor.service.SysAdministrationService;
import com.jyt.motor.vo.ResponseMod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO: 用户的权限及资源管理
 *
 * @version 1.0.0
 * @author: zhangWeiBin
 **/
@RestController
@RequestMapping("/motor")
@Slf4j
public class SysAdministrationController {
    /**
     * The Administration service.
     */
    @Autowired
    SysAdministrationService administrationService;

    /**
     * 获取所有目录
     *
     * @return meun
     */
    @RequestMapping("/sys/getMeun/v1.0")
    public ResponseMod getMeun() {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(administrationService.getMeun());
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("获取菜单异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 修改指定菜单
     *
     * @param menus the menus
     * @return response mod
     */
    @RequestMapping("/sys/updMeun/v1.0")
    public ResponseMod updMeun(SysMenus menus) {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(administrationService.updMeun(menus));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("修改菜单异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 添加菜单
     *
     * @param menus the menus
     * @return response mod
     */
    @RequestMapping("/sys/addMeun/v1.0")
    public ResponseMod addMeun(SysMenus menus) {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(administrationService.addMeun(menus));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("新增菜单异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 删除指定菜单
     *
     * @param menuId
     * @return
     */
    @RequestMapping("/sys/delMenu/v1.0")
    public ResponseMod delMenu(int menuId) {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(administrationService.delMenu(menuId));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("新增菜单异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 获取所有用户
     *
     * @return users
     */
    @RequestMapping("/sys/getUsers/v1.0")
    public ResponseMod getUsers() {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(administrationService.getUsers());
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("获取菜单异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 获取所有角色
     *
     * @return roles
     */
    @RequestMapping("/sys/getRoles/v1.0")
    public ResponseMod getRoles() {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(administrationService.getRoles());
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("获取角色异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 新增角色
     *
     * @param roles       the roles
     * @param userRolesId the user roles id
     * @return response mod
     */
    @RequestMapping("/sys/addRoles/v1.0")
    public ResponseMod addRoles(SysRoles roles, String userRolesId) {
        ResponseMod mod = new ResponseMod();
        try {
            if (!"sys".equals(userRolesId)) {
                throw new Exception("当前用户权限不足！！！");
            }
            mod.setData(administrationService.addRoles(roles));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("新增角色异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    @RequestMapping("/sys/updRoles/v1.0")
    public ResponseMod updRoles(SysRoles roles, String userRolesId) {
        ResponseMod mod = new ResponseMod();
        try {
            if (!"sys".equals(userRolesId)) {
                throw new Exception("当前用户权限不足！！！");
            }
            mod.setData(administrationService.updRoles(roles));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("新增角色异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    @RequestMapping("/sys/delRoles/v1.0")
    public ResponseMod delRoles(SysRoles roles, String userRolesId) {
        ResponseMod mod = new ResponseMod();
        try {
            if (!"sys".equals(userRolesId)) {
                throw new Exception("当前用户权限不足！！！");
            }
            if (!administrationService.canDelRoles(roles.getRoleId())) {
                mod.setData("当前角色中含有用户或菜单，不能删除");
                mod.setStateCode(ComConstant.FAILCODE);
                mod.setStateMsg(ComConstant.FAILMSG);
            } else {
                mod.setData(administrationService.updRoles(roles));
                mod.setStateCode(ComConstant.SUCCESSCODE);
                mod.setStateMsg(ComConstant.SUCCESSMSG);
            }
        } catch (Exception e) {
            log.error("新增角色异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 角色 对 菜单
     *
     * @param roleId      the role id
     * @param menuIds     the menu ids
     * @param userRolesId the user roles id
     * @return response mod
     */
    @RequestMapping("/sys/rolesToMeun/v1.0")
    public ResponseMod rolesToMeun(Integer roleId, String menuIds, String userRolesId) {
        ResponseMod mod = new ResponseMod();
        try {
            if (!"sys".equals(userRolesId)) {
                throw new Exception("当前用户角色权限不足以给角色添加菜单！！！");
            }
            mod.setData(administrationService.rolesToMenu(roleId, menuIds));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("角色添加菜单异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    //@RequestMapping("/sys/delRolesMeunExcept/v1.0")
    //public ResponseMod delRolesMenuExcept(Integer roleId, String menuIds, String userRolesId){
    //    ResponseMod mod = new ResponseMod();
    //    try {
    //        if (!"sys".equals(userRolesId)) {
    //            throw new Exception("当前用户角色权限不足以给角色添加菜单！！！");
    //        }
    //        mod.setData(administrationService.rolesToMeun(roleId,menuIds));
    //        mod.setStateCode(ComConstant.SUCCESSCODE);
    //        mod.setStateMsg(ComConstant.SUCCESSMSG);
    //    } catch (Exception e) {
    //        log.error("角色添加菜单异常！！", e);
    //        mod.setStateCode(ComConstant.FAILCODE);
    //        mod.setStateMsg(ComConstant.FAILMSG);
    //    }
    //    return mod;
    //}
}
