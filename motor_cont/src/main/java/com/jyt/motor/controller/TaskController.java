package com.jyt.motor.controller;

import com.jyt.motor.service.MoReportService;
import com.alibaba.fastjson.JSONObject;
import com.jyt.motor.service.ReceiveEBTService;
import com.jyt.motor.service.ReceiveIWIRService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.annotation.*;
import org.springframework.scheduling.config.CronTask;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.config.Task;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

@RestController
@RequestMapping("/ht_sensor/input")
@Configuration  //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling  // 2.开启定时任务
@PropertySource("classpath:/task-config.ini")
@Slf4j
public class TaskController implements SchedulingConfigurer {
    @Autowired
    ReceiveEBTService receiveEBTService;
    @Autowired
    MoReportService moReportService;

    @Autowired
    ReceiveIWIRService receiveIWIRService;

    @Value("${generateReport}")
    private String generateReport;
    @Value("${generateReportDaily}")
    private String generateReportDaily;
    @Value("${generateReportMonthly}")
    private String generateReportMonthly;


    /**
    *@Description: 系统定时任务 接收程序
    *@Param:
    *@return:
    *@Author: 黄国健
    *@date: 2020/7/7
    */
    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar)
    {
        taskRegistrar.setScheduler(taskExecutor());
        //EBT模块处理数据，定时器
        Runnable tasks = new Runnable() {
            @Override
            public void run() {
                receiveEBTService.saveData();
            }
        };
        Trigger triggers = new Trigger() {
            @Override
            public Date nextExecutionTime(TriggerContext triggerContext) {
                // 定时任务触发，修改定时任务的执行周期
                CronTrigger trigger = new CronTrigger("*/"+receiveEBTService.getSaveDataCron()+" * * * * ?");
                Date nextExecDate = trigger.nextExecutionTime(triggerContext);
                return nextExecDate;
            }
        };
        taskRegistrar.addTriggerTask(tasks, triggers);
        //EBT模块获取数据，定时器
        Runnable taskg = new Runnable() {
            @Override
            public void run() {
                receiveEBTService.getData();
            }
        };
        Trigger triggerg = new Trigger() {
            @Override
            public Date nextExecutionTime(TriggerContext triggerContext) {
                CronTrigger trigger = new CronTrigger("*/"+receiveEBTService.getGetDataCron()+" * * * * ?");
                Date nextExecDate = trigger.nextExecutionTime(triggerContext);
                return nextExecDate;
            }
        };
        taskRegistrar.addTriggerTask(taskg, triggerg);
        //IWIR模块处理数据，定时器
        Runnable tasks_ = new Runnable() {
            @Override
            public void run() {
                receiveIWIRService.saveData();
            }
        };
        Trigger triggers_ = new Trigger() {
            @Override
            public Date nextExecutionTime(TriggerContext triggerContext) {
                // 定时任务触发，修改定时任务的执行周期
                CronTrigger trigger = new CronTrigger("*/"+receiveIWIRService.getSaveDataCron()+" * * * * ?");
                Date nextExecDate = trigger.nextExecutionTime(triggerContext);
                return nextExecDate;
            }
        };
        taskRegistrar.addTriggerTask(tasks_, triggers_);
        //IWIR模块获取数据，定时器
        Runnable taskg_ = new Runnable() {
            @Override
            public void run() {
                receiveIWIRService.getData();
            }
        };
        Trigger triggerg_ = new Trigger() {
            @Override
            public Date nextExecutionTime(TriggerContext triggerContext) {
                CronTrigger trigger = new CronTrigger("*/"+receiveIWIRService.getGetDataCron()+" * * * * ?");
                Date nextExecDate = trigger.nextExecutionTime(triggerContext);
                return nextExecDate;
            }
        };
        taskRegistrar.addTriggerTask(taskg_, triggerg_);
        //生成报表数据，定时器
        Runnable taskh= new Runnable() {
            @Override
            public void run() {
                //产生小时表
                moReportService.generateReportHourly();
            }
        };
        Trigger triggerh = new Trigger() {
            @Override
            public Date nextExecutionTime(TriggerContext triggerContext) {
                CronTrigger trigger = new CronTrigger(generateReport);
                Date nextExecDate = trigger.nextExecutionTime(triggerContext);
                return nextExecDate;
            }
        };
        taskRegistrar.addTriggerTask(taskh, triggerh);
        Runnable taskd= new Runnable() {
            @Override
            public void run() {
                //产生天表
                moReportService.generateReportDaily();
            }
        };
        Trigger triggerd = new Trigger() {
            @Override
            public Date nextExecutionTime(TriggerContext triggerContext) {
                CronTrigger trigger = new CronTrigger(generateReportDaily);
                Date nextExecDate = trigger.nextExecutionTime(triggerContext);
                return nextExecDate;
            }
        };
        taskRegistrar.addTriggerTask(taskd, triggerd);
        Runnable taskm= new Runnable() {
            @Override
            public void run() {
                //产生月表
                moReportService.generateReportMonthly();
            }
        };
        Trigger triggerm = new Trigger() {
            @Override
            public Date nextExecutionTime(TriggerContext triggerContext) {
                CronTrigger trigger = new CronTrigger(generateReportMonthly);
                Date nextExecDate = trigger.nextExecutionTime(triggerContext);
                return nextExecDate;
            }
        };
        taskRegistrar.addTriggerTask(taskm, triggerm);
    }

    @Bean(destroyMethod = "shutdown")
    public Executor taskExecutor() {
        ScheduledExecutorService executor= Executors.newScheduledThreadPool(20);
        return executor; //指定线程池大小
    }

//    @Scheduled(cron = "${generateReport}")
//    public void generateReport() {
//        //产生小时表(不存在时产生)
//        moReportService.generateReportHourly();
//        System.out.println("generateReport......................");
//        //产生天表(不存在时产生)
//       // moReportService.generateReportDaily();
//        //产生月表(不存在时产生)
//    }

}
