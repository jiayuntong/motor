package com.jyt.motor.controller;

import com.jyt.motor.constant.ComConstant;
import com.jyt.motor.service.MoReportService;
import com.jyt.motor.service.impl.AlarmLampServiceImpSerialPort;
import com.jyt.motor.util.AlarmUtil;
import com.jyt.motor.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController()
@RequestMapping("/motor/monitor")
@Slf4j
public class MonitorController {
    @Autowired
    MoReportService moReportService;

    @Autowired
    AlarmLampServiceImpSerialPort alarmLampServiceImpSerialPort;

    @RequestMapping("/dashboard/v1.0")
    public ResponseMod Dashboard(@RequestParam(value = "periods", defaultValue = "1") Integer periods) {
        ResponseMod responseMod = new ResponseMod();
        try {
            Map<String, Object> result = new HashMap<>();
            DashboardStatisticMod statistic = moReportService.getStatisticInfo(periods);
            result.put("statisticInfo", statistic);
            List<DashboardAlertInfo> listAlert = moReportService.getAlertInfo(periods);
            result.put("exceptionLatest", listAlert);
            List<DashboardAreaMotors> listAreaMotor = moReportService.getAreaMotors(periods);
            result.put("data", listAreaMotor);
            responseMod.setData(result);
            responseMod.setStateCode(ComConstant.SUCCESSCODE);
            responseMod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception exc) {
            log.error("获取看板信息时发生错误", exc);
            responseMod.setStateCode(ComConstant.FAILCODE);
            responseMod.setStateMsg(ComConstant.FAILMSG);
        }
        return responseMod;
    }

    /**
     * 设置马达开线/保养
     *
     * @param periods   期别
     * @param moId      马达Id，-1表示所有马达
     * @param openState 1.开线，-1.关线
     * @return
     */
    @RequestMapping("/makeProcess/v1.0")
    public ResponseMod makeProcess(Integer periods, Integer moId, Integer openState) {
        ResponseMod responseMod = new ResponseMod();
        try {
            Integer r = moReportService.makeProcess(periods, moId, openState);
            responseMod.setData(r);
            responseMod.setStateCode(ComConstant.SUCCESSCODE);
            responseMod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception exc) {
            log.error("开线关线时发生错误", exc);
            responseMod.setData("开线关线时发生错误");
            responseMod.setStateCode(ComConstant.FAILCODE);
            responseMod.setStateMsg(ComConstant.FAILMSG);
        }
        return responseMod;
    }

    /**
     * @param openState 临时关闭报警灯
     * @return
     */
    @RequestMapping("/closeAlarmLight/v1.0")
    public ResponseMod setAlert(@RequestParam(value = "openState", defaultValue = "-1") Integer openState) {
        ResponseMod responseMod = new ResponseMod();
        try {
            moReportService.closeAlarmLamp();
            //alarmLampServiceImpSerialPort.closeAlarmLamp();
            responseMod.setData("OK");
            responseMod.setStateCode(ComConstant.SUCCESSCODE);
            responseMod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception exc) {
            log.error("开关报警灯时发生错误", exc);
            responseMod.setData("开关报警灯时发生错误");
            responseMod.setStateCode(ComConstant.FAILCODE);
            responseMod.setStateMsg(ComConstant.FAILMSG);
        }
        return responseMod;
    }

    @RequestMapping("/recent/v1.0")
    public ResponseMod recentRecord(Integer moId) {
        ResponseMod responseMod = new ResponseMod();
        try {
            List<DashboardRecentInfo> list = moReportService.getRecent5(moId);
            responseMod.setData(list);
            responseMod.setStateCode(ComConstant.SUCCESSCODE);
            responseMod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception exc) {
            log.error("显示最近几条记录时时发生错误", exc);
            responseMod.setData("显示最近几条记录时时发生错误");
            responseMod.setStateCode(ComConstant.FAILCODE);
            responseMod.setStateMsg(ComConstant.FAILMSG);
        }
        return responseMod;
    }

    @RequestMapping("/Alarm/v1.0")
    public void RandomAlarm() {
        ExecutorService executors = Executors.newCachedThreadPool();
        //alarmLampServiceImpSerialPort.setPortName("COM1");
        //alarmLampServiceImpSerialPort.openPort();
        for (int i = 0; i < 100; i++) {
            executors.submit(new Runnable() {
                @Override
                public void run() {
                    int i = RandomUtils.nextInt();
                    if (i % 2 == 0) {
                        //alarmLampServiceImpSerialPort.openAlarmLamp();
                        AlarmUtil.openAlarmLamp("COM1");
                    } else {
                        //alarmLampServiceImpSerialPort.closeAlarmLamp();
                        AlarmUtil.closeAlarmLamp("COM1");
                    }
                }
            });
        }
        //alarmLampServiceImpSerialPort.closePort();
    }

    @RequestMapping("/Alarm/v2.0")
    public void RandomAlarm2() {
        ExecutorService executors = Executors.newFixedThreadPool(100);
        for (int i = 0; i < 100; i++) {
            executors.submit(new Runnable() {
                @Override
                public void run() {
                    int i = RandomUtils.nextInt();
                    if (i % 2 == 0) {
                        log.info("开灯begin");
                        alarmLampServiceImpSerialPort.openAlarmLamp();
                        log.info("开灯");
                    } else {
                        log.info("关灯Begin");
                        alarmLampServiceImpSerialPort.closeAlarmLamp();
                        log.info("关灯");
                    }
                }
            });
        }
    }
}