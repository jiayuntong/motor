package com.jyt.motor.controller;

import com.jyt.motor.constant.ComConstant;
import com.jyt.motor.service.MoAlertRecService;
import com.jyt.motor.service.MoDataRecService;
import com.jyt.motor.service.MoReportService;
import com.jyt.motor.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.*;

@RestController()
@RequestMapping("/motor")
@Slf4j
public class ReportController {
    @Autowired
    MoDataRecService moDataRecService;
    @Autowired
    MoAlertRecService moAlertRecService;
    @Autowired
    MoReportService moReportService;

    SimpleDateFormat sfStandard = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @ResponseBody
    @RequestMapping("/mo_data_rec/select/v1.0")
    public ResponseMod dataRecSelect(CallParMotorsReport cp) {
        ResponseMod mod = new ResponseMod();
        try {
            Date dtFrom = sfStandard.parse(cp.getStartTime());
            Date dtTo = sfStandard.parse(cp.getEndTime());
            cp.setStartTime(sfStandard.format(dtFrom));
            cp.setEndTime(sfStandard.format(dtTo));
            //
            if (cp.getLimit() == null || cp.getLimit() <= 0) {
                cp.setLimit(10);
            }
            if (cp.getCurr() == null || cp.getCurr() <= 0) {
                cp.setCurr(1);
            }
            cp.setSqlLimit(cp.getLimit());
            cp.setSqlOffset((cp.getCurr() - 1) * cp.getLimit());
            //
            List<Integer> listMoId = new ArrayList<>();
            if (StringUtils.isNotBlank(cp.getMoIds())) {
                String[] ids = cp.getMoIds().split(",");
                for (String id : ids) {
                    if (StringUtils.isNumeric(id)) {
                        listMoId.add(Integer.valueOf(id));
                    }
                }
            }
            cp.setListMoIds(listMoId);
            //
            Map<String, Object> result = new HashMap<>();
            result.put("data", moDataRecService.moDataRecSelect(cp));
            result.put("total", moDataRecService.moDataRecCount(cp));
            result.put("limit", cp.getLimit());
            result.put("curr", cp.getCurr());
            mod.setData(result);
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("查询历史记录时发生异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 获取所有报警类别
     *
     * @return response mod
     */
    @RequestMapping("/mo_alert_connect/alertClass/v1.0")
    public ResponseMod alertClass() {
        ResponseMod mod = new ResponseMod();
        try {
            Map<Integer, String> map = moAlertRecService.alertClass();
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
            mod.setData(map);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 获取马达的报警记录
     *
     * @param parameter 参数类
     * @return response mod
     */
    @RequestMapping("/mo_alert_rec/select/v1.0")
    public ResponseMod moAlertRecSelect(CallParaMotorsExcept parameter) {
        ResponseMod mod = new ResponseMod();
        try {
            List<MoAlertRecView> moAlertAecList = moAlertRecService.moAlertRecSelect(parameter);
            Long count = moAlertRecService.moAlertRecCount(parameter);
            Map<String, Object> map = new HashMap<>();
            map.put("data", moAlertAecList);
            map.put("total", count);
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
            mod.setData(map);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    @RequestMapping("/chart/get/trend/v1.0")
    public ResponseMod queryReport(CallParaMotorsChart cp) {
        ResponseMod mod = new ResponseMod();
        try {
            LinkedHashMap map = moReportService.getTimeTrend(cp);
            mod.setData(map);
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }
}