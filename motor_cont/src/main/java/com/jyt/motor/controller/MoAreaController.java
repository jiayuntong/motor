package com.jyt.motor.controller;

import com.jyt.motor.constant.ComConstant;
import com.jyt.motor.model.MoArea;
import com.jyt.motor.model.MoConverter;
import com.jyt.motor.model.MoMonitors;
import com.jyt.motor.model.MoMotors;
import com.jyt.motor.service.MoAreaService;
import com.jyt.motor.service.MoConverterService;
import com.jyt.motor.service.MoMonitorsService;
import com.jyt.motor.service.MoMotorsService;
import com.jyt.motor.vo.MonitorView;
import com.jyt.motor.vo.ResponseMod;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/motor")
@Slf4j
public class MoAreaController {
    @Autowired
    MoAreaService moAreaService;
    @Autowired
    MoConverterService moConverterService;
    @Autowired
    MoMonitorsService moMonitorsService;
    @Autowired
    MoMotorsService moMotorsService;

    /**
     * 获取所有区域
     *
     * @return meun
     */
    @RequestMapping("/area_info/select/v1.0")
    public ResponseMod getConfigAll() {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(moAreaService.selectAll());
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("获取区域异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 新增区域
     *
     * @return meun
     */
    @RequestMapping("/area_info/addArea/v1.0")
    public ResponseMod ConfigInsert(MoArea moArea) {
        ResponseMod mod = new ResponseMod();
        try {
            if (StringUtils.isEmpty(moArea.getAreaName())
                    || StringUtils.isBlank(moArea.getAreaName())
            ) {
                mod.setData("存在空白项");
                mod.setStateCode(ComConstant.FAILCODE);
                mod.setStateMsg(ComConstant.FAILMSG);
                return mod;
            }
            if (moArea.getAreaPId() == null || moArea.getAreaPId() < 0) {
                moArea.setAreaId(0);
            }
            if (moArea.getSortSn() == null || moArea.getSortSn() < 0) {
                moArea.setSortSn(1);
            }
            mod.setData(moAreaService.insertSelective(moArea));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("获取区域异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 修改区域
     *
     * @return meun
     */
    @RequestMapping("/area_info/updateArea/v1.0")
    public ResponseMod ConfigUpdate(MoArea moArea) {
        ResponseMod mod = new ResponseMod();
        try {
            if (StringUtils.isEmpty(moArea.getAreaName())
                    || StringUtils.isBlank(moArea.getAreaName())
            ) {
                mod.setData("存在空白项");
                mod.setStateCode(ComConstant.FAILCODE);
                mod.setStateMsg(ComConstant.FAILMSG);
                return mod;
            }
            if (moArea.getAreaId() == null
                    || moArea.getAreaId() <= 0) {
                mod.setData("非法请求");
                mod.setStateCode(ComConstant.FAILCODE);
                mod.setStateMsg(ComConstant.FAILMSG);
                return mod;
            }
            if (moArea.getAreaPId() == null || moArea.getAreaPId() < 0) {
                moArea.setAreaPId(0);
            }
            if (moArea.getSortSn() == null || moArea.getSortSn() < 0) {
                moArea.setSortSn(1);
            }
            mod.setData(moAreaService.updateByPrimaryKeySelective(moArea));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("获取区域异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 删除区域
     *
     * @return meun
     */
    @RequestMapping("/area_info/delete/v1.0")
    public ResponseMod ConfigDelete(MoArea moArea) {
        ResponseMod mod = new ResponseMod();
        try {
            if (moArea.getAreaId() == null
                    || moArea.getAreaId() <= 0) {
                mod.setData("非法请求");
                mod.setStateCode(ComConstant.FAILCODE);
                mod.setStateMsg(ComConstant.FAILMSG);
                return mod;
            }
            mod.setData(moAreaService.deleteByPrimaryKey(moArea.getAreaId()));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("获取区域异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }


    /**
     * 获取所有转换器
     *
     * @return meun
     */
    @RequestMapping("/converter/select/v1.0")
    public ResponseMod getConverterAll() {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(moConverterService.selectAll());
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("获取转换器异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 新增转换器
     *
     * @return meun
     */
    @RequestMapping("/converter/add/v1.0")
    public ResponseMod ConverterInsert(MoConverter moConverter) {
        ResponseMod mod = new ResponseMod();
        try {
            if (moConverter.getInUse() == null || moConverter.getInUse() < 0) {
                moConverter.setInUse(1);
            }
            mod.setData(moConverterService.insertSelective(moConverter));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("获取区域异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 修改转换器
     *
     * @return meun
     */
    @RequestMapping("/converter/update/v1.0")
    public ResponseMod ConverterUpdate(MoConverter moConverter) {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(moConverterService.updateByPrimaryKeySelective(moConverter));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("获取区域异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 删除转换器
     *
     * @return meun
     */
    @RequestMapping("/converter/delete/v1.0")
    public ResponseMod ConverterDelete(MoConverter moConverter) {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(moConverterService.deleteByPrimaryKey(moConverter.getConverterId()));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("获取区域异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }


    /**
     * 获取所有马达
     *
     * @return meun
     */
    @RequestMapping("/mo_motors/select/v1.0")
    public ResponseMod getMotorAll() {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(moMotorsService.motorViewSelect());
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("获取马达异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 新增马达
     *
     * @return meun
     */
    @RequestMapping("/mo_motors/add/v1.0")
    public ResponseMod motorAdd(MoMotors moMotors) {
        ResponseMod mod = new ResponseMod();
        BigDecimal Fuyi = new BigDecimal(-1);
        try {
            if (moMotors.getUpperLimit() != null && moMotors.getUpperLimit().compareTo(Fuyi) != 0
                    && moMotors.getLowerLimit() != null && moMotors.getLowerLimit().compareTo(Fuyi) != 0) {
                int compare = moMotors.getUpperLimit().compareTo(moMotors.getLowerLimit());
                if (compare == -1) {
                    //上限低于下限
                    mod.setStateCode(ComConstant.FAILCODE);
                    mod.setStateMsg(ComConstant.FAILMSG);
                    mod.setData("上限值不能低于下限值");
                    return mod;
                }
            }
            mod.setData(moMotorsService.insertSelective(moMotors));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("新增马达异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 修改马达
     *
     * @return meun
     */
    @RequestMapping("/mo_motors/update/v1.0")
    public ResponseMod motorUpdate(MoMotors moMotors) {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(moMotorsService.updateByPrimaryKeySelective(moMotors));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("修改马达异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 删除马达
     *
     * @return meun
     */
    @RequestMapping("/mo_motors/delete/v1.0")
    public ResponseMod motorDelete(Integer moId) {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(moMotorsService.deleteByPrimaryKey(moId));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("删除马达异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    @RequestMapping("/mo_motors/selectMortor/v1.0")
    public ResponseMod selectMotors(Integer areaId) {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(moMotorsService.selectMortor(areaId));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("获取马达异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 给马达安装监测器
     *
     * @param moId
     * @param monitorId
     * @return
     */
    @RequestMapping("/mo_motors/install_monitor/v1.0")
    public ResponseMod monitorInstall(Integer moId, Integer monitorId) {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(moMotorsService.installMonitor(moId, monitorId));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("马达安装监测器异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 给马达卸载监测器
     *
     * @param moId
     * @return
     */
    @RequestMapping("/mo_motors/uninstall_monitor/v1.0")
    public ResponseMod monitorUninstall(Integer moId) {
        ResponseMod mod = new ResponseMod();
        try {
            if (moId == null || moId.intValue() <= 0) {
                mod.setStateCode(ComConstant.FAILCODE);
                mod.setStateMsg(ComConstant.FAILMSG);
                return mod;
            }
            mod.setData(moMotorsService.uninstallMonitor(moId));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("马达卸载监测器异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    @RequestMapping("/monitors/caninstalled/v1.0")
    public ResponseMod monitorCanInstalled() {
        ResponseMod mod = new ResponseMod();
        try {
            List<MonitorView> lists = moMonitorsService.canInstalled();
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
            mod.setData(lists);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
            mod.setData(null);
        }
        return mod;
    }

    /**
     * 获取所有监测器
     *
     * @return meun
     */
    @RequestMapping("/monitors/select2/v1.0")
    public ResponseMod getMonitorAll() {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(moMotorsService.monitorsSelect2());
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("获取监测器异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 新增监测器
     *
     * @return meun
     */
    @RequestMapping("/monitors/add/v1.0")
    public ResponseMod monitorAdd(MoMonitors moMonitors) {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(moMonitorsService.insertSelective(moMonitors));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("新增监测器异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 修改监测器
     *
     * @return meun
     */
    @RequestMapping("/monitors/update/v1.0")
    public ResponseMod monitorUpd(MoMonitors moMonitors) {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(moMonitorsService.updateByPrimaryKeySelective(moMonitors));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("修改监测器异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

    /**
     * 删除监测器
     *
     * @return meun
     */
    @RequestMapping("/monitors/delete/v1.0")
    public ResponseMod monitorDel(Integer monitorId) {
        ResponseMod mod = new ResponseMod();
        try {
            mod.setData(moMonitorsService.deleteByPrimaryKey(monitorId));
            mod.setStateCode(ComConstant.SUCCESSCODE);
            mod.setStateMsg(ComConstant.SUCCESSMSG);
        } catch (Exception e) {
            log.error("删除监测器异常！！", e);
            mod.setStateCode(ComConstant.FAILCODE);
            mod.setStateMsg(ComConstant.FAILMSG);
        }
        return mod;
    }

}
