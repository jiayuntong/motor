package com.jyt.motor.constant.context;


import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: ThreadStore
 * @Description: 线程内数据
 * @author: zhenxing.li1
 * @date: 2017年6月30日 上午10:18:53
 */
public class ThreadStore {
	private static final String REQUEST_KEY = "REQUEST";

	private static ThreadLocal<Map<String, Object>> props = new ThreadLocal<Map<String, Object>>() {
		protected synchronized Map<String, Object> initialValue() {
			return new HashMap<>();
		}
	};

	public static Map<String, Object> get() {
		return props.get();
	}

	public static HttpServletRequest getRequest() {
		return (HttpServletRequest) props.get().get(REQUEST_KEY);
	}

	public static HttpServletRequest setRequest(HttpServletRequest request) {
		return (HttpServletRequest) props.get().put(REQUEST_KEY, request);
	}
}
