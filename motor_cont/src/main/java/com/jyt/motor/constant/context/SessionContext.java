
package com.jyt.motor.constant.context;


import com.jyt.motor.constant.DefinesConst;
import com.jyt.motor.constant.session.ISession;
import com.jyt.motor.constant.session.Session;


public class SessionContext {

    /**
     * 初始化session上下文
     */
    private final static ThreadLocal<ISession<Object>> safeSession = new ThreadLocal<ISession<Object>>() {
        @Override
        protected ISession<Object> initialValue() {
            return new Session<Object>();
        }
    };

    private SessionContext() {

    }

    public static ISession<Object> getSession() {
        return safeSession.get();
    }

    /**
     * 设置真是session
     * setSession
     * @param session
     * @return void
     * @since:0.7
     */
    public static void setSession(javax.servlet.http.HttpSession session) {
        ISession<Object> sessionHold = safeSession.get();
        sessionHold.init(session);
    }

    /**
     * 清除ThreadLocal
     * remove
     * @return void
     * @since:0.7
     */
    public static void remove() {
        safeSession.get().init(null);
    }
    /**
     * 设置登陆用户Userid
     * setCurrentUser
     * @param user
     * @return void
     * @since:0.7
     */
    public static void setCurrentUser(CurrentUser user){
        safeSession.get().setObject(DefinesConst.KEY_USER_ID, user);
    }
    /**
     *
     * getCurrentUserId(用于记录操作人)
     *
     * @return
     * @return String
     * @exception
     *
     */
    public static CurrentUser getCurrentUser(){
            return  (CurrentUser) safeSession.get().getObject(DefinesConst.KEY_USER_ID);
    }


    /**
     * 使session失效
     * invalidateSession
     * @return void
     * @since:0.7
     */
    public static void invalidateSession() {
        if(safeSession.get()!=null){
            safeSession.get().invalidate();
        }
    }
}
