
package com.jyt.motor.constant;


public class DefinesConst {

    /**
     * tuser表id key定义
     */
    public static final String KEY_USER_ID               = "FRAMEWORK__KEY_USERID";

    /**
     * 页面返回码
     */
    public static final String RETURN_CODE               = "resCode";

    /**
     * 页面返回信息
     */
    public static final String RETURN_MESSAGE            = "resMsg";

    public static final String RETURN_RESULT             = "resResult" ; //true、false

    public static final boolean SUCCESS                  = true ;
    public static final boolean FAILURE                  = false ;

    public static final Integer SUCCESS_INTEGER          = 1 ;//成功


    public static final String OPERATION_TYPE_INSERT     = "01"; // 新增
    public static final String OPERATION_TYPE_MODIFY     = "02"; // 修改
    public static final String OPERATION_TYPE_QUERY      = "03"; // 查询
    public static final String OPERATION_TYPE_QUERY_CONTRACT              = "04"; // 查询合同信息（根据合同编号）
    public static final String OPERATION_TYPE_UPDATE_CONTRACT_STATUS      = "05"; // 更新合同状态（根据合同编号）
}
