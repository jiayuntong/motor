/**
 * 文件名：ISession.java
 * 版本信息：1.0
 * 日期：2015年7月14日-下午1:32:41
 */
package com.jyt.motor.constant.session;


/**
 * ISession
 *
 * @author qian.xu
 * @date 2015年7月14日 下午1:32:41
 * @version
 *
 */

public interface ISession<V> {

    /**
     * 设置真实session
     * init
     * @param session
     * @return void
     * @since:0.6
     */
    void init(javax.servlet.http.HttpSession session);

    /**
     * 设置session属性
     * setObject
     * @param k
     * @param v
     * @return void
     * @since:0.6
     */
    void setObject(String k, V v);

    /**
     * 读取session属性
     * getObject
     * @param k
     * @return
     * @return V
     * @since:0.6
     */
    V getObject(String k);

    /**
     * session失效
     * invalidate
     * @return void
     * @since:0.6
     */
    void invalidate();
    /**
     * 移除
     * removeByName(这里用一句话描述这个方法的作用)
     *
     * @param name
     * @return void
     * @exception
     *
     */
    void removeByName(String name);

    public javax.servlet.http.HttpSession getSession();
}
