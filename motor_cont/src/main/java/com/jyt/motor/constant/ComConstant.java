package com.jyt.motor.constant;

/**
 * 定义常量类
 */
public class ComConstant {

    //定义返回stateCode
    public static final int SUCCESSCODE = 0;
    public static final int FAILCODE = 1;

    //定义返回 stateMsg
    public static final String SUCCESSMSG = "操作成功！";
    public static final String FAILMSG = "操作失败！";

    /** 1.马达振动超标*/
    public static final int ALERTCLASS_MOTOR_OVERFLOW = 1;
    /** 2.马达振动超管制*/
    public static final int ALERTCLASS_MOTOR_OVERLIMIT = 2;
    /* 3.马达故障：PLC信号与马达信号不一致*/
    public static final int ALERTCLASS_MOTOR_EXCEPTION = 3;
    /* 4.马达长时间无数据*/
    public static final int ALERTCLASS_MOTER_OFFLINE = 4;
    //5.PLC报警位报警
    //public static final int ALERTCLASS_PLC_ALERT = 5;
    /*6.PLC断线*/
    public static final int ALERTCLASS_PLC_DISCONNECT = 6;
    /*7.震动信号断线*/
    public static final int ALERTCLASS_CONVERTER_DISCONNECT = 7;
    /**节点长时间200异常**/
    public static final int ALERTCLASS_TWO_HUNDRED = 8;

    //马达状态(0.离线, 0.未振动, 1.正常，2.异常，3.维修中)
    public static final int MOTOR_STATE_OFFLINE = 0;
    public static final int MOTOR_STATE_STOP = 0;
    public static final int MOTOR_STATE_NORMAL = 1;
    public static final int MOTOR_STATE_EXCEPTION = 2;
    public static final int MOTOR_STATE_MAINTAIN = 3;

    public static final String LIGHT_GRAY = "gray";
    public static final String LIGHT_GREEN = "green";
    public static final String LIGHT_RED = "red";
    public static final String LIGHT_BLUE = "blue";
    public static final String LIGHT_YELLOW = "yellow";
    //连续振动马达
    public static final int MOTOR_TYPE_CONTINUITY = 1;
    //间歇振动马达
    public static final int MOTOR_TYPE_INTERMITTENT = 2;

    //串口报警灯
    public static final String SERIAL_ALARM = "0";
    //网口报警灯
    public static final String NET_ALARM = "1";

    /******趋势类型（1时，2天，3周，4月，5年）******/
    public static final int CHART_TYPE_1 = 1;
    public static final int CHART_TYPE_2 = 2;
    public static final int CHART_TYPE_3 = 3;
    public static final int CHART_TYPE_4 = 4;
    public static final int CHART_TYPE_5 = 5;

    public static final int HOURLY_REPORT = 1;
    public static final int DAILY_REPORT = 2;
    public static final int MONTHLY_REPORT = 3;
}
