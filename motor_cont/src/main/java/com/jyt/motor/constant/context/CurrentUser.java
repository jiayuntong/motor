package com.jyt.motor.constant.context;



import com.jyt.motor.model.AuthMod;
import com.jyt.motor.model.SysUsers;

import java.io.Serializable;
import java.util.List;

public class CurrentUser extends SysUsers implements Serializable {

	private static final long serialVersionUID = -4319092058621840530L;

    /*
     * 用户权限集合
     */
    private List<AuthMod> userAuthList;

    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<AuthMod> getUserAuthList() {
        return userAuthList;
    }

    public void setUserAuthList(List<AuthMod> userAuthList) {
        this.userAuthList = userAuthList;
    }
}
