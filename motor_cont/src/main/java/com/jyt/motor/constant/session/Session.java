/**
 * 文件名：Session.java
 * 版本信息：1.0
 * 日期：2015年7月14日-下午1:33:06
 */
package com.jyt.motor.constant.session;

import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Map;


/**
 * Session
 *
 * @author qian.xu
 * @date 2015年7月14日 下午1:33:06
 * @version
 *
 */

public class Session<V> implements ISession<V> {

 private HttpSession session;

    /**
     *
     * @see com.deppon.foss.framework.server.web.session.ISession#setObject(String, Object)
     * setObject
     * @param k
     * @param v
     * @since:0.6
     */
    public void setObject(String k, V v) {
        //先验证设置的值是否被允许
//        validateSessionValue(v);
        session.setAttribute(k, v);
    }

    /**
     *
     * @see com.deppon.foss.framework.server.web.session.ISession#getObject(String)
     * getObject
     * @param k
     * @return
     * @since:0.6
     */
    @SuppressWarnings("unchecked")
    public V getObject(String k) {
        Object value = null;
        value = session.getAttribute(k);
        return (V) value;
    }

    /**
     *
     * @see com.deppon.foss.framework.server.web.session.ISession#init(HttpSession)
     * init
     * @param session
     * @since:0.6
     */
    public void init(HttpSession session) {
        this.session = session;
    }

    /**
     * 验证设置属性是否合法
     * validateSessionValue
     * @param v
     * @return void
     * @since:0.6
     */
    @SuppressWarnings("unchecked")
    void validateSessionValue(V v) {
        if (null == v)
            return;
        if (String.class == v.getClass())
            return;
        if (Long.class == v.getClass())
            return;
        if (Integer.class == v.getClass())
            return;
        if (java.util.Date.class == v.getClass())
            return;
        if(java.util.Locale.class == v.getClass()){
            return;
        }
        //SessionValue注解标注的放过
//        if (v.getClass().isAnnotationPresent(SessionValue.class))
//            return;
        //对于Collection及Map的遍历里面所有元素进行判断
        if (Collection.class.isAssignableFrom(v.getClass())) {
            Collection<?> collect = (Collection<?>) v;
            for (Object t : collect) {
                validateSessionValue((V) t);
            }
        }
        if (Map.class.isAssignableFrom(v.getClass())) {
            Map<?, ?> map = (Map<?, ?>) v;
            for (Map.Entry<?, ?> entry : map.entrySet()) {
                validateSessionValue((V) entry.getValue());
            }
        }
        throw new IllegalArgumentException();
    }

    /**
     *
     * @see com.deppon.foss.framework.server.web.session.ISession#invalidate()
     * invalidate
     * @since:0.6
     */

    public void invalidate() {
        session.invalidate();

    }

    /* (non-Javadoc)
     * @see com.zhaogang.pmb.webapp.framework.session.ISession#removeByName(java.lang.String)
     */

    public void removeByName(String name) {
        // TODO Auto-generated method stub
        session.removeAttribute(name);
    }

    public HttpSession getSession(){
        return this.session ;
    }
}
