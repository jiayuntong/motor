package com.jyt.motor.vo;

import com.jyt.motor.model.MoArea;

import java.util.List;

public class DashboardAreaMotors extends MoArea {
    List<DashboardMotorInfo> motorInfos;

    public List<DashboardMotorInfo> getMotorInfos() {
        return motorInfos;
    }

    public void setMotorInfos(List<DashboardMotorInfo> motorInfos) {
        this.motorInfos = motorInfos;
    }
}
