package com.jyt.motor.vo;

public class DashboardMotorInfo {
    Integer moId;
    String moName;
    Integer sortSn;
    Integer areaId;
    Integer moState;
    String moOpenState;
    String moHealthState;
    Integer moAlertId;

    public Integer getMoId() {
        return moId;
    }

    public void setMoId(Integer moId) {
        this.moId = moId;
    }

    public String getMoName() {
        return moName;
    }

    public void setMoName(String moName) {
        this.moName = moName;
    }

    public Integer getSortSn() {
        return sortSn;
    }

    public void setSortSn(Integer sortSn) {
        this.sortSn = sortSn;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getMoState() {
        return moState;
    }

    public void setMoState(Integer moState) {
        this.moState = moState;
    }

    public String getMoOpenState() {
        return moOpenState;
    }

    public void setMoOpenState(String moOpenState) {
        this.moOpenState = moOpenState;
    }

    public String getMoHealthState() {
        return moHealthState;
    }

    public void setMoHealthState(String moHealthState) {
        this.moHealthState = moHealthState;
    }

    public Integer getMoAlertId() {
        return moAlertId;
    }

    public void setMoAlertId(Integer moAlertId) {
        this.moAlertId = moAlertId;
    }
}
