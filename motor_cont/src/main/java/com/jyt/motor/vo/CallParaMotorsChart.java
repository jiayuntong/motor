package com.jyt.motor.vo;

public class CallParaMotorsChart {
    /**
     * 图表类型
     * 1.时图表
     * 2.日图表
     * 3.月图表
     */
    Integer trendType;
    /**
     * 开始时间
     */
    String startTime;
    /**
     * 结束时间
     */
    String endTime;
    /**
     * 区域ID
     */
    Integer areaId;
    /**
     * 马达IDs，以逗号分隔的MoId
     */
    String moIds;

    public Integer getTrendType() {
        return trendType;
    }

    public void setTrendType(Integer trendType) {
        this.trendType = trendType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getMoIds() {
        return moIds;
    }

    public void setMoIds(String moIds) {
        this.moIds = moIds;
    }
}
