package com.jyt.motor.vo;

import java.math.BigDecimal;
import java.util.Date;

public class MoMonitorsAndConverter {
    private Integer monitorId;

    private Integer gauge;

    private String strGauge;

    private Integer valueFrom;

    private Integer valueTo;

    private Integer converterId;

    private Integer bitSn;

    private Date installTime;

    private String strInstallTime;

    private BigDecimal fixBase;

    private Integer inputFrom;

    private Integer inputTo;

    private Integer monitorState;

    private String monitorLight;

    private String converterAddr;

    private String monitorShow;

    public Integer getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Integer monitorId) {
        this.monitorId = monitorId;
    }

    public Integer getGauge() {
        return gauge;
    }

    public void setGauge(Integer gauge) {
        this.gauge = gauge;
    }

    public String getStrGauge() {
        return strGauge;
    }

    public void setStrGauge(String strGauge) {
        this.strGauge = strGauge;
    }

    public Integer getValueFrom() {
        return valueFrom;
    }

    public void setValueFrom(Integer valueFrom) {
        this.valueFrom = valueFrom;
    }

    public Integer getValueTo() {
        return valueTo;
    }

    public void setValueTo(Integer valueTo) {
        this.valueTo = valueTo;
    }

    public Integer getConverterId() {
        return converterId;
    }

    public void setConverterId(Integer converterId) {
        this.converterId = converterId;
    }

    public Integer getBitSn() {
        return bitSn;
    }

    public void setBitSn(Integer bitSn) {
        this.bitSn = bitSn;
    }

    public Date getInstallTime() {
        return installTime;
    }

    public void setInstallTime(Date installTime) {
        this.installTime = installTime;
    }

    public String getStrInstallTime() {
        return strInstallTime;
    }

    public void setStrInstallTime(String strInstallTime) {
        this.strInstallTime = strInstallTime;
    }

    public BigDecimal getFixBase() {
        return fixBase;
    }

    public void setFixBase(BigDecimal fixBase) {
        this.fixBase = fixBase;
    }

    public Integer getInputFrom() {
        return inputFrom;
    }

    public void setInputFrom(Integer inputFrom) {
        this.inputFrom = inputFrom;
    }

    public Integer getInputTo() {
        return inputTo;
    }

    public void setInputTo(Integer inputTo) {
        this.inputTo = inputTo;
    }

    public Integer getMonitorState() {
        return monitorState;
    }

    public void setMonitorState(Integer monitorState) {
        this.monitorState = monitorState;
    }

    public String getMonitorLight() {
        return monitorLight;
    }

    public void setMonitorLight(String monitorLight) {
        this.monitorLight = monitorLight;
    }

    public String getConverterAddr() {
        return converterAddr;
    }

    public void setConverterAddr(String converterAddr) {
        this.converterAddr = converterAddr;
    }

    public String getMonitorShow() {
        return monitorShow;
    }

    public void setMonitorShow(String monitorShow) {
        this.monitorShow = monitorShow;
    }
}
