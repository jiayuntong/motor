package com.jyt.motor.vo;

import com.jyt.motor.model.MoMotors;

import java.math.BigDecimal;

public class MotorView extends MoMotors {
    String areaName;
    int converterId;
    String converterAddr;
    //int monitorId;
    int valueFrom;
    int valueTo;
    int bitSn;
    BigDecimal fixBase;
    int areaSort;
    String monitorShow;

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public int getConverterId() {
        return converterId;
    }

    public void setConverterId(int converterId) {
        this.converterId = converterId;
    }

    public String getConverterAddr() {
        return converterAddr;
    }

    public void setConverterAddr(String converterAddr) {
        this.converterAddr = converterAddr;
    }

    public int getValueFrom() {
        return valueFrom;
    }

    public void setValueFrom(int valueFrom) {
        this.valueFrom = valueFrom;
    }

    public int getValueTo() {
        return valueTo;
    }

    public void setValueTo(int valueTo) {
        this.valueTo = valueTo;
    }

    public int getBitSn() {
        return bitSn;
    }

    public void setBitSn(int bitSn) {
        this.bitSn = bitSn;
    }

    public BigDecimal getFixBase() {
        return fixBase;
    }

    public void setFixBase(BigDecimal fixBase) {
        this.fixBase = fixBase;
    }

    public int getAreaSort() {
        return areaSort;
    }

    public void setAreaSort(int areaSort) {
        this.areaSort = areaSort;
    }

    public String getMonitorShow() {
        return monitorShow;
    }

    public void setMonitorShow(String monitorShow) {
        this.monitorShow = monitorShow;
    }
}
