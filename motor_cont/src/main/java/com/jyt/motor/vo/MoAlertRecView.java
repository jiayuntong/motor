package com.jyt.motor.vo;

import com.jyt.motor.model.MoAlertRec;

public class MoAlertRecView extends MoAlertRec {
    String strAlertClass;
    String strHasDealed;
    String strDealTime;
    String strAlertTime;

    public String getStrAlertClass() {
        return strAlertClass;
    }

    public void setStrAlertClass(String strAlertClass) {
        this.strAlertClass = strAlertClass;
    }

    public String getStrHasDealed() {
        return strHasDealed;
    }

    public void setStrHasDealed(String strHasDealed) {
        this.strHasDealed = strHasDealed;
    }

    public String getStrDealTime() {
        return strDealTime;
    }

    public void setStrDealTime(String strDealTime) {
        this.strDealTime = strDealTime;
    }

    public String getStrAlertTime() {
        return strAlertTime;
    }

    public void setStrAlertTime(String strAlertTime) {
        this.strAlertTime = strAlertTime;
    }
}
