package com.jyt.motor.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 封装返回类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseMod {
    //状态码 0正常  1错误
    private int stateCode;
    //状态信息
    private String stateMsg;
    //返回对象
    private Object data;

    public int getStateCode() {
        return stateCode;
    }

    public void setStateCode(int stateCode) {
        this.stateCode = stateCode;
    }

    public String getStateMsg() {
        return stateMsg;
    }

    public void setStateMsg(String stateMsg) {
        this.stateMsg = stateMsg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
