package com.jyt.motor.vo;

public class CallPaging {
    /*每页行数*/
    Integer limit;
    /*当前页次*/
    Integer curr;
    Integer sqlLimit;
    Integer sqlOffset;

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getCurr() {
        return curr;
    }

    public void setCurr(Integer curr) {
        this.curr = curr;
    }

    public Integer getSqlLimit() {
        return sqlLimit;
    }

    public void setSqlLimit(Integer sqlLimit) {
        this.sqlLimit = sqlLimit;
    }

    public Integer getSqlOffset() {
        return sqlOffset;
    }

    public void setSqlOffset(Integer sqlOffset) {
        this.sqlOffset = sqlOffset;
    }
}
