package com.jyt.motor.vo;

import com.jyt.motor.model.MoMonitors;

public class MonitorView extends MoMonitors {
    String converterAddr;

    public String getConverterAddr() {
        return converterAddr;
    }

    public void setConverterAddr(String converterAddr) {
        this.converterAddr = converterAddr;
    }
}