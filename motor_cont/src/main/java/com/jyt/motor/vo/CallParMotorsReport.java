package com.jyt.motor.vo;

import java.util.List;

public class CallParMotorsReport {
    /**
     * 开始时间
     */
    String startTime;
    /**
     * 结束时间
     */
    String endTime;
    /**
     * 区域ID
     */
    Integer areaId;
    /**
     * 马达IDs，以逗号分隔的MoId
     */
    String moIds;
    /**
     * 当前页次
     */
    Integer curr;
    /**
     * 每页行数
     */
    Integer limit;
    //依据上方计算后得值
    List<Integer> listMoIds;
    Integer sqlLimit;
    Integer sqlOffset;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getMoIds() {
        return moIds;
    }

    public void setMoIds(String moIds) {
        this.moIds = moIds;
    }

    public Integer getCurr() {
        return curr;
    }

    public void setCurr(Integer curr) {
        this.curr = curr;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public List<Integer> getListMoIds() {
        return listMoIds;
    }

    public void setListMoIds(List<Integer> listMoIds) {
        this.listMoIds = listMoIds;
    }

    public Integer getSqlLimit() {
        return sqlLimit;
    }

    public void setSqlLimit(Integer sqlLimit) {
        this.sqlLimit = sqlLimit;
    }

    public Integer getSqlOffset() {
        return sqlOffset;
    }

    public void setSqlOffset(Integer sqlOffset) {
        this.sqlOffset = sqlOffset;
    }
}
