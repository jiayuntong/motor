package com.jyt.motor.vo;

import java.math.BigDecimal;

public class ChartView {
    Integer moId;
    String moName;
    Integer areaId;
    String areaName;
    BigDecimal rockData;
    Integer chartType;
    String chartTypeShow;
    String recTime;
    float maintainRate;
    float failureRate;
    float dynamicRate;

    BigDecimal upperLimitInner;
    BigDecimal lowerLimitInner;

    public Integer getMoId() {
        return moId;
    }

    public void setMoId(Integer moId) {
        this.moId = moId;
    }

    public String getMoName() {
        return moName;
    }

    public void setMoName(String moName) {
        this.moName = moName;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public BigDecimal getRockData() {
        return rockData;
    }

    public void setRockData(BigDecimal rockData) {
        this.rockData = rockData;
    }

    public Integer getChartType() {
        return chartType;
    }

    public void setChartType(Integer chartType) {
        this.chartType = chartType;
    }

    public String getChartTypeShow() {
        return chartTypeShow;
    }

    public void setChartTypeShow(String chartTypeShow) {
        this.chartTypeShow = chartTypeShow;
    }

    public String getRecTime() {
        return recTime;
    }

    public void setRecTime(String recTime) {
        this.recTime = recTime;
    }

    public float getMaintainRate() {
        return maintainRate;
    }

    public void setMaintainRate(float maintainRate) {
        this.maintainRate = maintainRate;
    }

    public float getFailureRate() {
        return failureRate;
    }

    public void setFailureRate(float failureRate) {
        this.failureRate = failureRate;
    }

    public float getDynamicRate() {
        return dynamicRate;
    }

    public void setDynamicRate(float dynamicRate) {
        this.dynamicRate = dynamicRate;
    }

    public BigDecimal getUpperLimitInner() {
        return upperLimitInner;
    }

    public void setUpperLimitInner(BigDecimal upperLimitInner) {
        this.upperLimitInner = upperLimitInner;
    }

    public BigDecimal getLowerLimitInner() {
        return lowerLimitInner;
    }

    public void setLowerLimitInner(BigDecimal lowerLimitInner) {
        this.lowerLimitInner = lowerLimitInner;
    }
}
