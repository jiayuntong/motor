package com.jyt.motor.vo;

import java.math.BigDecimal;
import java.util.Date;

public class DashboardRecentInfo {
    Long recId;
    Integer moId;
    Date recTime;
    BigDecimal rockData;
    BigDecimal upperLimit;
    BigDecimal lowerLimit;

    public Long getRecId() {
        return recId;
    }

    public void setRecId(Long recId) {
        this.recId = recId;
    }

    public Integer getMoId() {
        return moId;
    }

    public void setMoId(Integer moId) {
        this.moId = moId;
    }

    public Date getRecTime() {
        return recTime;
    }

    public void setRecTime(Date recTime) {
        this.recTime = recTime;
    }

    public BigDecimal getRockData() {
        return rockData;
    }

    public void setRockData(BigDecimal rockData) {
        this.rockData = rockData;
    }

    public BigDecimal getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(BigDecimal upperLimit) {
        this.upperLimit = upperLimit;
    }

    public BigDecimal getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(BigDecimal lowerLimit) {
        this.lowerLimit = lowerLimit;
    }
}
