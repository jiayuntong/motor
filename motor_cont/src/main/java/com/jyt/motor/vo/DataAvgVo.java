package com.jyt.motor.vo;

import java.math.BigDecimal;

public class DataAvgVo {
    private Integer moId;
    private BigDecimal avgData;
    private BigDecimal avgUpperLimit;
    private BigDecimal avgLowerLimit;
    private float maintainRate;
    private float failureRate;
    private float dynamicRate;

    public Integer getMoId() {
        return moId;
    }

    public void setMoId(Integer moId) {
        this.moId = moId;
    }

    public BigDecimal getAvgData() {
        return avgData;
    }

    public void setAvgData(BigDecimal avgData) {
        this.avgData = avgData;
    }

    public BigDecimal getAvgUpperLimit() {
        return avgUpperLimit;
    }

    public void setAvgUpperLimit(BigDecimal avgUpperLimit) {
        this.avgUpperLimit = avgUpperLimit;
    }

    public BigDecimal getAvgLowerLimit() {
        return avgLowerLimit;
    }

    public void setAvgLowerLimit(BigDecimal avgLowerLimit) {
        this.avgLowerLimit = avgLowerLimit;
    }

    public float getMaintainRate() {
        return maintainRate;
    }

    public void setMaintainRate(float maintainRate) {
        this.maintainRate = maintainRate;
    }

    public float getFailureRate() {
        return failureRate;
    }

    public void setFailureRate(float failureRate) {
        this.failureRate = failureRate;
    }

    public float getDynamicRate() {
        return dynamicRate;
    }

    public void setDynamicRate(float dynamicRate) {
        this.dynamicRate = dynamicRate;
    }
}
