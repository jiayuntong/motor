package com.jyt.motor.vo;

public class DashboardStatisticMod {
    /**
     * 马达总数
     */
    Integer totalNum;
    /* *  离线数目 * */
    Integer offlineNum;
    /**
     * 正常数目
     */
    Integer normalNum;
    /**
     * 异常数目
     */
    Integer exceptNum;
    /**
     * 维护数目
     **/
    Integer maintainNum;
    /**
     * 正常率
     */
    String normalRate;

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public Integer getOfflineNum() {
        return offlineNum;
    }

    public void setOfflineNum(Integer offlineNum) {
        this.offlineNum = offlineNum;
    }

    public Integer getNormalNum() {
        return normalNum;
    }

    public void setNormalNum(Integer normalNum) {
        this.normalNum = normalNum;
    }

    public Integer getExceptNum() {
        return exceptNum;
    }

    public void setExceptNum(Integer exceptNum) {
        this.exceptNum = exceptNum;
    }

    public Integer getMaintainNum() {
        return maintainNum;
    }

    public void setMaintainNum(Integer maintainNum) {
        this.maintainNum = maintainNum;
    }

    public String getNormalRate() {
        return normalRate;
    }

    public void setNormalRate(String normalRate) {
        this.normalRate = normalRate;
    }
}
