package com.jyt.motor.vo;

import com.jyt.motor.model.SysRoles;

import java.util.List;

public class SysRoleVoShow extends SysRoles {
    private List<String> menuTitle;

    public List<String> getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(List<String> menuTitle) {
        this.menuTitle = menuTitle;
    }
}
