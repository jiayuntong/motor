package com.jyt.motor.vo;

public class SysRolesVo {
    private Integer roleId;
    private String menuTitle;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    @Override
    public String toString() {
        return "SysRolesVo{" +
                "roleId=" + roleId +
                ", menuTitle='" + menuTitle + '\'' +
                '}';
    }
}
