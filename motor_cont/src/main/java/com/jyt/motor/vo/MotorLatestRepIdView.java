package com.jyt.motor.vo;

public class MotorLatestRepIdView {
    private Integer moId;
    private String moName;
    private Integer areaId;
    private String areaName;
    private Long latestHourlyId;
    private Long latestDailyId;
    private Long latestMonthlyId;

    public Integer getMoId() {
        return moId;
    }

    public void setMoId(Integer moId) {
        this.moId = moId;
    }

    public String getMoName() {
        return moName;
    }

    public void setMoName(String moName) {
        this.moName = moName;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Long getLatestHourlyId() {
        return latestHourlyId;
    }

    public void setLatestHourlyId(Long latestHourlyId) {
        this.latestHourlyId = latestHourlyId;
    }

    public Long getLatestDailyId() {
        return latestDailyId;
    }

    public void setLatestDailyId(Long latestDailyId) {
        this.latestDailyId = latestDailyId;
    }

    public Long getLatestMonthlyId() {
        return latestMonthlyId;
    }

    public void setLatestMonthlyId(Long latestMonthlyId) {
        this.latestMonthlyId = latestMonthlyId;
    }
}
