package com.jyt.motor.vo;

public class CallParaMotorsExcept extends CallPaging {
    /**
     * 开始时间
     */
    String startTime;
    /**
     * 结束时间
     */
    String endTime;
    /**
     * 处理状态
     */
    Integer hasDealed;
    /**
     * 区域ID
     */
    Integer areaId;
    /**
     * 马达ID
     */
    Integer moId;
    /**
     * 报警类别
     */
    Integer alertId;
    /**
     * 马达或者检测器
     * 如果等于1判断为检测器
     * 如果等于2判断为马达
     * 如果等于3判断为全部
     */
    int exType;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getHasDealed() {
        return hasDealed;
    }

    public void setHasDealed(Integer hasDealed) {
        this.hasDealed = hasDealed;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getMoId() {
        return moId;
    }

    public void setMoId(Integer moId) {
        this.moId = moId;
    }

    public Integer getAlertId() {
        return alertId;
    }

    public void setAlertId(Integer alertId) {
        this.alertId = alertId;
    }

    public int getExType() {
        return exType;
    }

    public void setExType(int exType) {
        this.exType = exType;
    }

}