package com.jyt.motor.service.impl;

import com.jyt.motor.dao.MoDataReportMapper;
import com.jyt.motor.model.MoDataReport;
import com.jyt.motor.service.MoDataReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MoDataReportServiceImp implements MoDataReportService {
    @Autowired
    MoDataReportMapper moDataReportMapper;
    @Override
    public int insert(MoDataReport record) {
        return moDataReportMapper.insert(record);
    }

    @Override
    public int insertSelective(MoDataReport record) {
        return moDataReportMapper.insertSelective(record);
    }

    @Override
    public int updateByPrimaryKey(MoDataReport record) {
        return moDataReportMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(MoDataReport record) {
        return moDataReportMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int deleteByPrimaryKey(Long id) {
        return moDataReportMapper.deleteByPrimaryKey(id);
    }

    @Override
    public MoDataReport selectByPrimaryKey(Long imId) {
        return moDataReportMapper.selectByPrimaryKey(imId);
    }

    @Override
    public List<MoDataReport> selectAll() {
        return moDataReportMapper.selectAll();
    }
}
