package com.jyt.motor.service;

import com.jyt.motor.model.MoMonitors;
import com.jyt.motor.vo.MonitorView;

import java.util.List;

public interface MoMonitorsService {

    int insert(MoMonitors record);

    int insertSelective(MoMonitors record);

    int updateByPrimaryKey(MoMonitors record);

    int updateByPrimaryKeySelective(MoMonitors record);

    int deleteByPrimaryKey(java.lang.Integer id);

    MoMonitors selectByPrimaryKey(java.lang.Integer imId);

    List<MoMonitors> selectAll();

    List<MonitorView> canInstalled();

}
