package com.jyt.motor.service.impl;

import com.jyt.motor.dao.MoDataRecMapper;
import com.jyt.motor.model.MoDataRec;
import com.jyt.motor.service.MoDataRecService;
import com.jyt.motor.vo.CallParMotorsReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MoDataRecServiceImp implements MoDataRecService {
    @Autowired
    MoDataRecMapper moDataRecMapper;

    @Override
    public int insert(MoDataRec record) {
        return moDataRecMapper.insert(record);
    }

    @Override
    public int insertSelective(MoDataRec record) {
        return moDataRecMapper.insertSelective(record);
    }

    @Override
    public int updateByPrimaryKey(MoDataRec record) {
        return moDataRecMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(MoDataRec record) {
        return moDataRecMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int deleteByPrimaryKey(Long id) {
        return moDataRecMapper.deleteByPrimaryKey(id);
    }

    @Override
    public MoDataRec selectByPrimaryKey(Long recId) {
        return moDataRecMapper.selectByPrimaryKey(recId);
    }

    @Override
    public List<MoDataRec> selectAll() {
        return moDataRecMapper.selectAll();
    }

    @Override
    public List<MoDataRec> moDataRecSelect(CallParMotorsReport parameter) {
        return moDataRecMapper.moDataRecSelect(parameter);
    }

    @Override
    public Integer moDataRecCount(CallParMotorsReport parameter){
        return moDataRecMapper.moDataRecCount(parameter);
    }

    @Override
    public List<MoDataRec> selectDataRecs(List<Integer> list) {
        return moDataRecMapper.selectDataRec(list);
    }

}
