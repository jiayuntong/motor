package com.jyt.motor.service;

import com.jyt.motor.vo.*;

import java.util.LinkedHashMap;
import java.util.List;

public interface MoReportService {
    LinkedHashMap getTimeTrend(CallParaMotorsChart callPara);

    DashboardStatisticMod getStatisticInfo(Integer periods);

    List<DashboardAlertInfo> getAlertInfo(Integer periods);

    List<DashboardAreaMotors> getAreaMotors(Integer periods);

    Integer makeProcess(Integer periods, Integer moId, Integer openState);

    Integer closeAlarmLamp();

    List<DashboardRecentInfo> getRecent5(Integer moId);

    void generateReportHourly();

    void generateReportDaily();

    void generateReportMonthly();

}
