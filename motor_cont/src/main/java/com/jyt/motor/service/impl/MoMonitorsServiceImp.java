package com.jyt.motor.service.impl;

import com.jyt.motor.dao.MoMonitorsMapper;
import com.jyt.motor.model.MoMonitors;
import com.jyt.motor.service.MoMonitorsService;
import com.jyt.motor.vo.MonitorView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MoMonitorsServiceImp implements MoMonitorsService {
    @Autowired
    MoMonitorsMapper moMonitorsMapper;

    @Override
    public int insert(MoMonitors record) {
        return moMonitorsMapper.insert(record);
    }

    @Override
    public int insertSelective(MoMonitors record) {
        return moMonitorsMapper.insertSelective(record);
    }

    @Override
    public int updateByPrimaryKey(MoMonitors record) {
        return moMonitorsMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(MoMonitors record) {
        return moMonitorsMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return moMonitorsMapper.deleteByPrimaryKey(id);
    }

    @Override
    public MoMonitors selectByPrimaryKey(Integer imId) {
        return moMonitorsMapper.selectByPrimaryKey(imId);
    }

    @Override
    public List<MoMonitors> selectAll() {
        return moMonitorsMapper.selectAll();
    }

    @Override
    public List<MonitorView> canInstalled() {
        return moMonitorsMapper.canInstalled();
    }

}
