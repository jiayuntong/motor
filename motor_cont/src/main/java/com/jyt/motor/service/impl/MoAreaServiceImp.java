package com.jyt.motor.service.impl;

import com.jyt.motor.dao.MoAreaMapper;
import com.jyt.motor.model.MoArea;
import com.jyt.motor.service.MoAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MoAreaServiceImp implements MoAreaService {
    @Autowired
    MoAreaMapper moAreaMapper;
    @Override
    public int insert(MoArea record) {
        return moAreaMapper.insert(record);
    }

    @Override
    public int insertSelective(MoArea record) {
        return moAreaMapper.insertSelective(record);
    }

    @Override
    public int updateByPrimaryKey(MoArea record) {
        return moAreaMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(MoArea record) {
        return moAreaMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return moAreaMapper.deleteByPrimaryKey(id);
    }

    @Override
    public MoArea selectByPrimaryKey(Integer imId) {
        return moAreaMapper.selectByPrimaryKey(imId);
    }

    @Override
    public List<MoArea> selectAll() {
        return moAreaMapper.selectAll();
    }
}
