package com.jyt.motor.service;

public interface AlarmLampService {
    void openAlarmLamp();
    void closeAlarmLamp();
}
