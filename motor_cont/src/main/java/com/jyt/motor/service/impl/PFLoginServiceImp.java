package com.jyt.motor.service.impl;

import com.jyt.motor.dao.SysUsersMapper;
import com.jyt.motor.model.AuthMod;
import com.jyt.motor.model.SysMenus;
import com.jyt.motor.model.SysUsers;
import com.jyt.motor.service.PFLoginsService;
import com.jyt.motor.util.SHA256Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PFLoginServiceImp implements PFLoginsService {
    @Autowired
    SysUsersMapper sysUsersMapper;
    public static final Logger logger = LoggerFactory.getLogger(PFLoginsService.class);

    @Override
    public Map<String, Object> login(SysUsers sysUsers) {
        Map<String, Object> map = new HashMap<>();
        SysUsers user = sysUsersMapper.getUser(sysUsers.getUserName());
        String pwd = SHA256Util.getSHA256StrJava(sysUsers.getUserPwd());
        System.out.println(pwd);
        Boolean flag = false;
        if (sysUsers.getUserName() != null && pwd != null) {
            flag = pwd.equals(user.getUserPwd());
        } else
            flag = false;
        if (flag) {
            Map<String, Object> mapAuth = findTree(user.getUserId());
            List<String> listOperation = sysUsersMapper.getOperation(user.getRoleId());
            map.put("userId", user.getUserId());
            map.put("mapAuth", mapAuth);
            map.put("listOperation", listOperation);
            map.put("isLogin", true);
        } else {
            map.put("isLogin", false);
        }
        return map;
    }

    /**
     * 排序
     *
     * @return
     */
    public Comparator<SysMenus> order() {
        Comparator<SysMenus> comparator = new Comparator<SysMenus>() {
            @Override
            public int
            compare(SysMenus o1, SysMenus o2) {
                if
                (o1.getMenuSort() != o2.getMenuSort()) {
                    return o1.getMenuSort() - o2.getMenuSort();
                }
                return 0;
            }
        };
        return comparator;
    }

    /**
     * 生成树
     *
     * @return
     */
    public Map<String, Object> findTree(Integer UserId) {

        Map<String, Object> data = new HashMap<>();

        try {
            //查询用户所有菜单
            //List<SysMenus> allMenu = menuDao.findTree();
            List<AuthMod> allMenu = sysUsersMapper.getAuth(UserId);

            //保存根节点
            List<AuthMod> rootMenu = new ArrayList<>();

            for (AuthMod nav : allMenu) {
                //父节点是0的，为根节点
                if (nav.getpMenuId() == 0) {
                    rootMenu.add(nav);
                }

            }

            //为根菜单设置子菜单，getChild是递归调用的
            for (AuthMod nav : rootMenu) {

                //获取跟节点下的所有子节点  使用getChild方法
                List<AuthMod> childList = getChild(nav.getMenuId(), allMenu);

                //给根节点设置子节点
                nav.setLists(childList);

            }

            //输出构建菜单结构
            data.put("success", "true");

            data.put("list", rootMenu);

            return data;

        } catch (Exception e) {
            logger.error(e.getMessage(), e);

            data.put("success", "false");

            data.put("list", new ArrayList());

            return data;

        }

    }

    /**
     * 获取子菜单
     *
     * @param id      父节点id
     * @param allMenu 所有菜单列表
     * @return 每个根节点下，所有子菜单列表
     */
    public List<AuthMod> getChild(Integer id, List<AuthMod> allMenu) {

        //子菜单
        List<AuthMod> childList = new ArrayList<>();


        for (AuthMod nav : allMenu) {
            //遍历所有节点，将所有菜单的父ID与传过来的节点id比较
            //相等说明：为该根节点的子节点
            if (nav.getpMenuId() == id) {

                childList.add(nav);

            }

        }

        //递归
        for (AuthMod nav : childList) {

            nav.setLists(getChild(nav.getMenuId(), allMenu));

        }
        //排序
        //如果节点没有子节点，返回一个空 List（递归退出）
        if (childList.size() == 0) {

            return new ArrayList<AuthMod>();

        }

        return childList;

    }

}