package com.jyt.motor.service;

import com.jyt.motor.model.SysUsers;
import sun.misc.Cleaner;

import java.util.List;

public interface ReceiveEBTService {

    boolean getData();

    boolean saveData();

    void clearConf();

    int getSaveDataCron();

    void clearSaveDataCron();

    int getGetDataCron();

    void clearGetDataCron();

    void clearMobiles();
}
