package com.jyt.motor.service;

import com.jyt.motor.model.MoConverter;

import java.util.List;

public interface MoConverterService {

    int insert(MoConverter record);

    int insertSelective(MoConverter record);

    int updateByPrimaryKey(MoConverter record);

    int updateByPrimaryKeySelective(MoConverter record);

    int deleteByPrimaryKey(java.lang.Integer id);

    MoConverter selectByPrimaryKey(java.lang.Integer imId);

    List<MoConverter> selectAll();

}
