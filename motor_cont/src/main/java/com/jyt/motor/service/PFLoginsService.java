package com.jyt.motor.service;

import com.jyt.motor.model.SysUsers;

import java.util.Map;

public interface PFLoginsService {

    Map<String, Object> login(SysUsers sysUsers);
}
