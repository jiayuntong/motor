package com.jyt.motor.service.impl;

import com.jyt.motor.dao.SysConfigMapper;
import com.jyt.motor.model.SysConfig;
import com.jyt.motor.service.SysConfigService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class SysConfigServiceImp implements SysConfigService {
    @Autowired
    SysConfigMapper sysConfigMapper;

    @Override
    public int insert(SysConfig record) {
        return sysConfigMapper.insert(record);
    }

    @Override
    public int insertSelective(SysConfig record) {
        return sysConfigMapper.insertSelective(record);
    }

    @Override
    public int updateByPrimaryKey(SysConfig record) {
        return sysConfigMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(SysConfig record) {
        return sysConfigMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return sysConfigMapper.deleteByPrimaryKey(id);
    }

    @Override
    public SysConfig selectByPrimaryKey(Integer id) {
        return sysConfigMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<SysConfig> selectAll() {
        return sysConfigMapper.selectAll();
    }
}
