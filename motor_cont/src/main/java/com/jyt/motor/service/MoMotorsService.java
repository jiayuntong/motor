package com.jyt.motor.service;

import com.jyt.motor.model.MoMotors;
import com.jyt.motor.vo.MoMonitorsAndConverter;
import com.jyt.motor.vo.MotorView;
import com.jyt.motor.vo.ResponseMod;

import java.util.List;

public interface MoMotorsService {

    int insert(MoMotors record);

    int insertSelective(MoMotors record);

    int updateByPrimaryKey(MoMotors record);

    int updateByPrimaryKeySelective(MoMotors record);

    int deleteByPrimaryKey(java.lang.Integer id);

    MoMotors selectByPrimaryKey(java.lang.Integer imId);

    List<MoMotors> selectAll();

    List<MotorView> motorViewSelect();

    ResponseMod installMonitor(int moId, int motorId);

    ResponseMod uninstallMonitor(int moId);

    List<MoMonitorsAndConverter> monitorsSelect2();

    List<MoMotors> selectMortor(Integer areaId);

}
