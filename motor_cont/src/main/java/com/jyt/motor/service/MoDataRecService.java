package com.jyt.motor.service;

import com.jyt.motor.model.MoDataRec;
import com.jyt.motor.vo.CallParMotorsReport;

import java.util.List;

public interface MoDataRecService {

    int insert(MoDataRec record);

    int insertSelective(MoDataRec record);

    int updateByPrimaryKey(MoDataRec record);

    int updateByPrimaryKeySelective(MoDataRec record);

    int deleteByPrimaryKey(java.lang.Long id);

    MoDataRec selectByPrimaryKey(java.lang.Long recId);

    List<MoDataRec> selectAll();

    List<MoDataRec> moDataRecSelect(CallParMotorsReport parameter);

    Integer moDataRecCount(CallParMotorsReport parameter);

    List<MoDataRec> selectDataRecs(List<Integer> list);

}
