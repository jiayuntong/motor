package com.jyt.motor.service.impl;

import com.jyt.motor.dao.SysAdministrationMapper;
import com.jyt.motor.dao.SysUsersMapper;
import com.jyt.motor.model.SysMenus;
import com.jyt.motor.model.SysRoles;
import com.jyt.motor.model.SysUsers;
import com.jyt.motor.service.SysAdministrationService;
import com.jyt.motor.vo.SysRoleVoShow;
import com.jyt.motor.vo.SysRolesVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SysAdministrationServiceImp implements SysAdministrationService {

    @Autowired
    SysAdministrationMapper administrationMapper;
    @Autowired
    SysUsersMapper sysUsersMapper;

    @Override
    public List<SysMenus> getMeun() {
        return administrationMapper.getMeun();
    }

    @Override
    public String updMeun(SysMenus menus) {
        Integer rs = administrationMapper.updMeun(menus);
        if (0 > rs) {
            return "FAIL";
        }
        return "SUCCES";
    }

    @Override
    public String addMeun(SysMenus menus) {
        Integer rs = administrationMapper.addMeun(menus);
        if (0 > rs) {
            return "FAIL";
        }
        return "SUCCES";
    }

    @Override
    public String delMenu(int menuId) {
        int i = administrationMapper.delMenu(menuId);
        return (i > 0 ? "SUCCES" : "FAIL");
    }

    @Override
    public List<SysRoleVoShow> getRoles() {
        List<SysRoles> roles = administrationMapper.getRoles();
        Map<Integer, SysRoleVoShow> map = new HashMap<>();

        if (roles != null) {
            for (SysRoles role : roles) {
                SysRoleVoShow show = new SysRoleVoShow();
                show.setRoleId(role.getRoleId());
                show.setRoleName(role.getRoleName());
                show.setRoleRemark(role.getRoleRemark());
                show.setMenuTitle(new ArrayList<>());
                map.put(role.getRoleId(), show);
            }
            List<SysRolesVo> vos = administrationMapper.getRoleIdToMeun(roles);
            if (vos != null) {
                for (SysRolesVo srvo : vos) {
                    SysRoleVoShow retvo = map.get(srvo.getRoleId());
                    List<String> list = retvo.getMenuTitle();
                    list.add(srvo.getMenuTitle());
                }
            }
        }
        return new ArrayList<>(map.values());
    }

    @Override
    public String addRoles(SysRoles roles) {
        Integer rs = administrationMapper.addRoles(roles);
        if (0 > rs) {
            return "FAIL";
        }
        return "SUCCES";
    }

    @Override
    public String updRoles(SysRoles roles) {
        Integer rs = administrationMapper.updRoles(roles);
        if (0 > rs) {
            return "FAIL";
        }
        return "SUCCES";
    }

    @Override
    public boolean canDelRoles(int roleId) {
        //有用户无法删除
        List<SysUsers> ll = sysUsersMapper.getAllFlowMeterUsers(roleId);
        if (ll.size() > 0) {
            return false;
        }
        //有菜单无法删除
        List<SysRoles> roleids = new ArrayList<>();
        SysRoles sr = new SysRoles();
        sr.setRoleId(roleId);
        roleids.add(sr);
        List<SysRolesVo> ll2 = administrationMapper.getRoleIdToMeun(roleids);
        if (ll2.size() > 0) {
            return false;
        }
        return true;
    }

    @Override
    public String delRoles(int roleId) {
        Integer rs = administrationMapper.delRoles(roleId);
        if (0 > rs) {
            return "FAIL";
        }
        return "SUCCES";
    }

    @Override
    public String rolesToMenu(Integer roleId, String menuIds) {
        String[] menuId;
        if (StringUtils.isEmpty(menuIds)) {
            return "请选择需要添加的菜单！！！";
        } else if (!StringUtils.isEmpty(menuIds)) {
            menuId = menuIds.split(",");
            administrationMapper.rolesToMeun(roleId, menuId);
            List<Integer> list = administrationMapper.exceptMenusOfRole(roleId, menuId);
            if (list.size() > 0) {
                Integer[] temp = list.toArray(new Integer[list.size()]);
                administrationMapper.delRoleMenu(roleId, temp);
            }
        }
        return "SUCCES";
    }

    @Override
    public List<SysUsers> getUsers() {
        return administrationMapper.getUsers();
    }
}
