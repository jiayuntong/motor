package com.jyt.motor.service;

import com.jyt.motor.model.MoDataReport;

import java.util.List;

public interface MoDataReportService {

    int insert(MoDataReport record);

    int insertSelective(MoDataReport record);

    int updateByPrimaryKey(MoDataReport record);

    int updateByPrimaryKeySelective(MoDataReport record);

    int deleteByPrimaryKey(java.lang.Long id);

    MoDataReport selectByPrimaryKey(java.lang.Long imId);

    List<MoDataReport> selectAll();

}
