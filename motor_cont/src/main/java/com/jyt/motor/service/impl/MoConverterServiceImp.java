package com.jyt.motor.service.impl;

import com.jyt.motor.dao.MoConverterMapper;
import com.jyt.motor.model.MoConverter;
import com.jyt.motor.service.MoConverterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MoConverterServiceImp implements MoConverterService {
    @Autowired
    MoConverterMapper moConverterMapper;
    @Override
    public int insert(MoConverter record) {
        return moConverterMapper.insert(record);
    }

    @Override
    public int insertSelective(MoConverter record) {
        return moConverterMapper.insertSelective(record);
    }

    @Override
    public int updateByPrimaryKey(MoConverter record) {
        return moConverterMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(MoConverter record) {
        return moConverterMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return moConverterMapper.deleteByPrimaryKey(id);
    }

    @Override
    public MoConverter selectByPrimaryKey(Integer imId) {
        return moConverterMapper.selectByPrimaryKey(imId);
    }

    @Override
    public List<MoConverter> selectAll() {
        return moConverterMapper.selectAll();
    }
}
