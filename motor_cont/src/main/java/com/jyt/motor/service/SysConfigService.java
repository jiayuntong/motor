package com.jyt.motor.service;

import java.util.List;
import com.jyt.motor.model.SysConfig;

public interface SysConfigService {

    int insert(SysConfig record);

    int insertSelective(SysConfig record);

    int updateByPrimaryKey(SysConfig record);

    int updateByPrimaryKeySelective(SysConfig record);

    int deleteByPrimaryKey(java.lang.Integer id);

    SysConfig selectByPrimaryKey(java.lang.Integer id);

    List<SysConfig> selectAll();

}
