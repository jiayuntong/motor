package com.jyt.motor.service.impl;

import com.jyt.motor.dao.MoAlertRecMapper;
import com.jyt.motor.model.MoAlertRec;
import com.jyt.motor.service.MoAlertRecService;
import com.jyt.motor.vo.CallPaging;
import com.jyt.motor.vo.CallParaMotorsExcept;
import com.jyt.motor.vo.MoAlertRecView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.jyt.motor.constant.ComConstant.*;

@Service
public class MoAlertRecServiceImp implements MoAlertRecService {
    @Autowired
    MoAlertRecMapper moAlertRecMapper;

    @Override
    public int insert(MoAlertRec record) {
        return moAlertRecMapper.insert(record);
    }

    @Override
    public int insertSelective(MoAlertRec record) {
        return moAlertRecMapper.insertSelective(record);
    }

    @Override
    public int updateByPrimaryKey(MoAlertRec record) {
        return moAlertRecMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(MoAlertRec record) {
        return moAlertRecMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return moAlertRecMapper.deleteByPrimaryKey(id);
    }

    @Override
    public MoAlertRec selectByPrimaryKey(Integer imId) {
        return moAlertRecMapper.selectByPrimaryKey(imId);
    }

    @Override
    public List<MoAlertRec> selectAll() {
        return moAlertRecMapper.selectAll();
    }

    @Override
    public Map<Integer, String> alertClass() {
        Map<Integer, String> map = new HashMap<>();
        map.put(ALERTCLASS_MOTOR_OVERFLOW, "马达超标");
        map.put(ALERTCLASS_MOTOR_OVERLIMIT, "马达超管制");
        //PLC与马达信号不一致
        map.put(ALERTCLASS_MOTOR_EXCEPTION, "马达故障");
        map.put(ALERTCLASS_TWO_HUNDRED, "数据异常");
        //map.put(ALERTCLASS_PLC_EXCEPTION,"马达异常或线路异常");
        //map.put(ALERTCLASS_PLC_ALERT, "PLC异常或线路异常");
        //map.put(ALERTCLASS_PLC_DISCONNECT, "plc断线");
        //map.put(ALERTCLASS_CONVERTER_DISCONNECT, "震动信号断线");
        //map.put(ALERTCLASS_MOTER_OFFLINE, "马达长时间无数据");
        //map.put(11,"马达监测器离线");
        //map.put(12,"PLC监测器离线");

        return map;
    }

    private void initSqlLimit(CallPaging parameter) {
        if (parameter.getLimit() == null || parameter.getLimit() <= 0) {
            parameter.setLimit(10);
        }
        if (parameter.getCurr() == null || parameter.getCurr() <= 0) {
            parameter.setCurr(1);
        }
        parameter.setSqlLimit(parameter.getLimit());
        parameter.setSqlOffset((parameter.getCurr() - 1) * parameter.getLimit());
        //
    }

    @Override
    public List<MoAlertRecView> moAlertRecSelect(CallParaMotorsExcept parameter) {
        List<MoAlertRecView> alertAecList = null;
        //
        initSqlLimit(parameter);
        //
        alertAecList = moAlertRecMapper.moAlertRecSelect(parameter);

        return alertAecList;
    }

    @Override
    public Long moAlertRecCount(CallParaMotorsExcept parameter) {
        //
        initSqlLimit(parameter);
        Long r = moAlertRecMapper.moAlertRecCount(parameter);
        return r;
    }
}