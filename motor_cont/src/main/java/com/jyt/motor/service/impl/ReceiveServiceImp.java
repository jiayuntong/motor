package com.jyt.motor.service.impl;

import com.jyt.motor.util.SqlServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

/**
 * @program: motor_cont
 * @description: 接收程序公共类
 * @author: guojian huang
 * @create: 2020-07-07 11:37
 */
public class ReceiveServiceImp {

    @Value("${motor.receiveDataTimeout}")
    public int receiveDataTimeout;

    protected int saveDataCron;

    protected int getDataCron;

    @Value("${motor.isAlarmOffline}")
    public int isAlarmOffline;

    protected String mobiles;

}
