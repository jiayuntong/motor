package com.jyt.motor.service;

import com.jyt.motor.model.MoArea;

import java.util.List;

public interface MoAreaService {

    int insert(MoArea record);

    int insertSelective(MoArea record);

    int updateByPrimaryKey(MoArea record);

    int updateByPrimaryKeySelective(MoArea record);

    int deleteByPrimaryKey(java.lang.Integer id);

    MoArea selectByPrimaryKey(java.lang.Integer imId);

    List<MoArea> selectAll();

}
