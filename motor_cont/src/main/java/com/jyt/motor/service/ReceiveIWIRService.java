package com.jyt.motor.service;

public interface ReceiveIWIRService {

    boolean getData();

    boolean saveData();

    void clearConf();

    int getSaveDataCron();

    void clearSaveDataCron();

    int getGetDataCron();

    void clearGetDataCron();

    void clearMobiles();
}
