package com.jyt.motor.service;

import com.jyt.motor.model.MoAlertRec;
import com.jyt.motor.vo.CallParaMotorsExcept;
import com.jyt.motor.vo.MoAlertRecView;

import java.util.List;
import java.util.Map;

public interface MoAlertRecService {

    int insert(MoAlertRec record);

    int insertSelective(MoAlertRec record);

    int updateByPrimaryKey(MoAlertRec record);

    int updateByPrimaryKeySelective(MoAlertRec record);

    int deleteByPrimaryKey(java.lang.Integer id);

    MoAlertRec selectByPrimaryKey(java.lang.Integer imId);

    List<MoAlertRec> selectAll();

    /**
     * 所有异常的类别
     *
     * @return
     */
    Map<Integer, String> alertClass();

    List<MoAlertRecView> moAlertRecSelect(CallParaMotorsExcept parameter);

    Long moAlertRecCount(CallParaMotorsExcept parameter);
}
