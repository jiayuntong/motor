package com.jyt.motor.service;

import com.jyt.motor.model.SysUsers;

import java.util.List;

public interface PFUsersService {

    List<SysUsers> getAll();

    SysUsers getUserName(String userName);

    void setChgPwd(String userName, String newPwd);

    void setChgPwd(Integer userId, String newPwd);

    void userAdd(SysUsers sysUsers);

    SysUsers selectByPrimaryKey(Integer userId);

    void updateSysUsers(SysUsers sysUsers);

    void addSysUser(SysUsers user);

    void delSysUser(SysUsers user);

}
