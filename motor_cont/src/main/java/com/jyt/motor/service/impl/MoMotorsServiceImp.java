package com.jyt.motor.service.impl;

import com.jyt.motor.constant.ComConstant;
import com.jyt.motor.dao.MoMonitorsMapper;
import com.jyt.motor.dao.MoMotorsMapper;
import com.jyt.motor.model.MoMonitors;
import com.jyt.motor.model.MoMotors;
import com.jyt.motor.service.MoMotorsService;
import com.jyt.motor.vo.MoMonitorsAndConverter;
import com.jyt.motor.vo.MotorView;
import com.jyt.motor.vo.ResponseMod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;

@Service
public class MoMotorsServiceImp implements MoMotorsService {
    @Autowired
    MoMotorsMapper moMotorsMapper;
    @Autowired
    MoMonitorsMapper moMonitorsMapper;

    @Override
    public int insert(MoMotors record) {
        return moMotorsMapper.insertSelective(record);
    }

    @Override
    public int insertSelective(MoMotors record) {
        return moMotorsMapper.insertSelective(record);
    }

    @Override
    public int updateByPrimaryKey(MoMotors record) {
        return moMotorsMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKeySelective(MoMotors record) {
        return moMotorsMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return moMotorsMapper.deleteByPrimaryKey(id);
    }

    @Override
    public MoMotors selectByPrimaryKey(Integer imId) {
        return moMotorsMapper.selectByPrimaryKey(imId);
    }

    @Override
    public List<MoMotors> selectAll() {
        return moMotorsMapper.motorsSelect();
    }

    @Override
    public List<MotorView> motorViewSelect() {
        List<MotorView> motorsShow = moMotorsMapper.motorViewsSelect();
        for (MotorView motorView : motorsShow) {
            if (!StringUtils.isEmpty(motorView.getConverterAddr()) && motorView.getBitSn() != 0) {
                motorView.setMonitorShow("地址" + motorView.getConverterAddr() + "-" + "第" + motorView.getBitSn() + "信号位");
            }
        }
        return motorsShow;
    }

    @Override
    public ResponseMod installMonitor(int moId, int motorId) {
        ResponseMod response = new ResponseMod();

        MoMotors motor = moMotorsMapper.selectByPrimaryKey(moId);
        if (motor == null) {
            response.setStateCode(ComConstant.FAILCODE);
            response.setStateMsg("参数值不正确");
            return response;
        }
        if (motor.getMonitorId() != null && motor.getMonitorId() > 0) {
            response.setStateCode(ComConstant.FAILCODE);
            response.setStateMsg("马达已安装有监测器");
            return response;
        }
        MoMonitors monitor = moMonitorsMapper.selectByPrimaryKey(motorId);
        if (monitor == null) {
            response.setStateCode(ComConstant.FAILCODE);
            response.setStateMsg("参数值不正确");
            return response;
        }
        MoMotors upd = new MoMotors();
        upd.setMoId(motor.getMoId());
        upd.setMonitorId(monitor.getMonitorId());
        moMotorsMapper.updateByPrimaryKeySelective(upd);
        //
        response.setStateCode(ComConstant.SUCCESSCODE);
        response.setStateMsg(ComConstant.SUCCESSMSG);

        return response;
    }

    @Override
    public ResponseMod uninstallMonitor(int moId) {
        ResponseMod response = new ResponseMod();
        //
        MoMotors upd = new MoMotors();
        upd.setMoId(moId);
        upd.setMonitorId(0);
        moMotorsMapper.updateByPrimaryKeySelective(upd);
        //
        response.setStateCode(ComConstant.SUCCESSCODE);
        response.setStateMsg(ComConstant.SUCCESSMSG);

        return response;
    }

    @Override
    public List<MoMonitorsAndConverter> monitorsSelect2() {
        List<MoMonitorsAndConverter> monitors = moMonitorsMapper.motorsSelect2();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        for (MoMonitorsAndConverter moni : monitors) {
            if (moni.getGauge() == 1) {
                moni.setStrGauge("小量程");
            } else if (moni.getGauge() == 2) {
                moni.setStrGauge("大量程");
            }

            if (moni.getInstallTime() != null) {
                moni.setStrInstallTime(df.format(moni.getInstallTime()));
            }
            if (!StringUtils.isEmpty(moni.getConverterAddr()) && moni.getBitSn() != 0) {
                moni.setMonitorShow("地址" + moni.getConverterAddr() + "-" + "第" + moni.getBitSn() + "信号位");
            }
        }
        return monitors;
    }

    @Override
    public List<MoMotors> selectMortor(Integer areaId) {
        return moMotorsMapper.selectMortor(areaId);
    }

}
