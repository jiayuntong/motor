package com.jyt.motor.service.impl;

import com.jyt.motor.dao.SysUsersMapper;
import com.jyt.motor.model.SysUsers;
import com.jyt.motor.service.PFUsersService;
import com.jyt.motor.util.SHA256Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
//@Transactionalpropagation = Propagation.REQUIRES_NEW)
public class PFUserServiceImp implements PFUsersService {

    @Autowired
    SysUsersMapper sysUsersMapper;

    @Override
    public List<SysUsers> getAll() {
        return sysUsersMapper.getAll();
    }

    @Override
    public SysUsers getUserName(String userName) {
        return sysUsersMapper.getUser(userName);
    }

    @Override
    public void setChgPwd(String userName, String newPwd) {
        SysUsers users = new SysUsers();
        users.setUserPwd(SHA256Util.getSHA256StrJava(newPwd));
        users.setUserName(userName);
        sysUsersMapper.setChgPwd(users);
    }

    @Override
    public void setChgPwd(Integer userId, String newPwd) {
        SysUsers users = new SysUsers();
        users.setUserPwd(SHA256Util.getSHA256StrJava(newPwd));
        users.setUserId(userId);
        sysUsersMapper.setChgPwdByUserId(users);
    }

    @Override
    public void userAdd(SysUsers sysUsers) {
        sysUsers.setUserPwd(SHA256Util.getSHA256StrJava(sysUsers.getUserPwd()));
        sysUsersMapper.insertSelective(sysUsers);
    }

    @Override
    public SysUsers selectByPrimaryKey(Integer userId) {
        return sysUsersMapper.selectByPrimaryKey(userId);
    }

    @Override
    public void updateSysUsers(SysUsers user) {
        sysUsersMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public void addSysUser(SysUsers user) {
        sysUsersMapper.insertSelective(user);
    }

    @Override
    public void delSysUser(SysUsers user) {
        sysUsersMapper.deleteByPrimaryKey(user.getUserId());
    }

}
