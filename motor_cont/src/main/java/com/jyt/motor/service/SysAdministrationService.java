package com.jyt.motor.service;

import com.jyt.motor.model.SysMenus;
import com.jyt.motor.model.SysRoles;
import com.jyt.motor.model.SysUsers;
import com.jyt.motor.vo.ResponseMod;
import com.jyt.motor.vo.SysRoleVoShow;
import com.jyt.motor.vo.SysRolesVo;

import java.util.List;
import java.util.Map;

public interface SysAdministrationService {
    List<SysMenus> getMeun();

    String updMeun(SysMenus menu);

    String addMeun(SysMenus menus);

    String delMenu(int menuId);

    List<SysRoleVoShow> getRoles();

    String addRoles(SysRoles roles);

    String updRoles(SysRoles roles);

    boolean canDelRoles(int roleId);

    String delRoles(int roleId);

    String rolesToMenu(Integer roleId, String menuIds);

    List<SysUsers> getUsers();
}
