package com.jyt.motor.test;

import com.jyt.motor.util.DateUtils;
import com.jyt.motor.util.SHA256Util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TestSHA256 {

    public static void main(String[] args) {

        String pw = "zwb1230..";


        String SHA = SHA256Util.getSHA256StrJava(pw);
        System.err.println(SHA);

        SimpleDateFormat s = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Date z = DateUtils.rollDay(new Date(), -3);//开始时间
        Date w = DateUtils.rollDay(new Date(), -1);//结束时间
        Date n = DateUtils.rollDay(new Date(), -1);//当前时间

        String sz = s.format(z);
        String sw = s.format(w);
        String sn = s.format(n);


        Boolean b = DateUtils.belongCalendar(n, z, w);
        System.out.println("b:" + b + ",\nsz:" + sz + ",\nsw:" + sw + ",\nsn:" + sn);

        if (b) {
            System.out.println("sn存在");
        }else {
            System.out.println("sn不存在");
        }


        System.out.println(DateUtils.rollDay(new Date(), -3));
    }

}
