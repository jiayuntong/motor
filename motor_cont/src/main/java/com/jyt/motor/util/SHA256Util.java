package com.jyt.motor.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA256Util {
    public static final Logger logger = LoggerFactory.getLogger(SHA256Util.class);
/**
 　　* 利用java原生的摘要实现SHA256加密
 　　* @param str 加密后的报文
 　　* @return
 　　*/
        public static String getSHA256StrJava(String str){
            MessageDigest messageDigest;
            String encodeStr = "";
            try {
                messageDigest = MessageDigest.getInstance("SHA-256");
                messageDigest.update(str.getBytes("UTF-8"));
                encodeStr = byte2Hex(messageDigest.digest());
                logger.info("password is null");

            } catch (NoSuchAlgorithmException e) {
                logger.error(e.getMessage(), e);
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                logger.error(e.getMessage(), e);
                e.printStackTrace();
            }catch ( NullPointerException e) {
                logger.error(e.getMessage(), e);
                System.err.println("password is null");
            }
            return encodeStr;
        }
        /**
     　　* 将byte转为16进制
     　　* @param bytes
     　　* @return
     　　*/
        private static String byte2Hex(byte[] bytes){
            StringBuffer stringBuffer = new StringBuffer();
            String temp = null;
            for (int i=0;i<bytes.length;i++){
                temp = Integer.toHexString(bytes[i] & 0xFF);
                if (temp.length()==1){
                    //1得到一位的进行补0操作
                    stringBuffer.append("0");
                }
                    stringBuffer.append(temp);
            }
            return stringBuffer.toString();
        }

}