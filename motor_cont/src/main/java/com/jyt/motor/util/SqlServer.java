package com.jyt.motor.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @program: motor_cont
 * @description:
 * @author: guojian huang
 * @create: 2020-08-24 13:42
 */
@Slf4j
@Component
public class SqlServer {
    @Value("${motor.isOpenShortMessage}")
    private String isOpenShortMessage;
    @Value("${motor.jdbc-url}")
    private String url;
    @Value("${motor.username}")
    private String username;
    @Value("${motor.password}")
    private String password;
    @Value("${motor.driverClassName}")
    private String driverClassName;

    public synchronized boolean addNotes(String mobiles, String content) {
        if(isOpenShortMessage.trim().equals("0")||StringUtils.isBlank(mobiles)||StringUtils.isBlank(content))
        {
            return true;
        }
        boolean result = false;
        //短信平台SQLserver
        try {
            Class.forName(driverClassName);
            Connection dbConn = null;
            dbConn = DriverManager.getConnection(url, username, password);
            PreparedStatement stmt = null;
            stmt = dbConn.prepareStatement("insert into [MIS_APCB].[dbo].[Msg_notes]" +
                    "        ([UserName],[Mobiles],[Content],[SubmitTime],[status],[type])" +
                    "      values" +
                    "        ('震動馬達報警',?,?,getdate(),0,'震動馬達');");
            stmt.setString(1, mobiles);
            stmt.setString(2, content);
            result = stmt.execute();
            log.info("result:-"+result+"-.insert into [MIS_APCB].[dbo].[Msg_notes]" +
                    "        ([UserName],[Mobiles],[Content],[SubmitTime],[status],[type])" +
                    "      values" +
                    "        ('震動馬達報警','"+mobiles+"','"+content+"',getdate(),0,'震動馬達');");
            stmt.close();
            dbConn.close();
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } catch (SQLException throwables) {
            log.error(throwables.getMessage());
            throwables.printStackTrace();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }


}
