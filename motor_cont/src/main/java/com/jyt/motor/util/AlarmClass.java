package com.jyt.motor.util;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;

public class AlarmClass {
    private static Log log = LogFactory.getLog(AlarmClass.class);

    private CommPortIdentifier commPortIdentifier;
    private SerialPort serialPort;
    private OutputStream outputStream;
    private String appName = "串口通讯开启报警";
    private int timeout = 2000;
    private boolean isPortOpen = false;

    public void selectPort(String serialPort) {
        this.commPortIdentifier = null;
        CommPortIdentifier cpi;
        @SuppressWarnings("rawtypes")
        Enumeration en = CommPortIdentifier.getPortIdentifiers();
        while (en.hasMoreElements()) {
            cpi = (CommPortIdentifier) en.nextElement();
            if (cpi.getPortType() == CommPortIdentifier.PORT_SERIAL
                    && cpi.getName().equals(serialPort)) {
                this.commPortIdentifier = cpi;
                break;
            }
        }
        try {
            openPort();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openPort() {
        if (commPortIdentifier == null) {
            isPortOpen = false;
        } else {
            try {
                serialPort = (SerialPort) commPortIdentifier.open(appName, timeout);
                isPortOpen = true;
            } catch (PortInUseException e) {
                throw new RuntimeException("端口" + commPortIdentifier.getName() + "正在使用中");
            } catch (Exception e) {
                log.error(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void checkPort() {
        if (commPortIdentifier == null) {
            throw new RuntimeException("没有可用的端口");
        }
        if (serialPort == null) {
            throw new RuntimeException("没有可用的串口对象");
        }
    }

    public void write(byte[] message) {
        checkPort();
        try {
            outputStream = new BufferedOutputStream(serialPort.getOutputStream());
        } catch (IOException e) {
            isPortOpen = false;
            throw new RuntimeException("获取串口的输出流出错：" + e.getMessage());
        }
        try {
            outputStream.write(message);
        } catch (IOException e) {
            isPortOpen = false;
            throw new RuntimeException("向端口" + commPortIdentifier.getName() + "发送信息时出错：" + e.getMessage());
        } finally {
            try {
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void close() {
        isPortOpen = false;
        serialPort.close();
        serialPort = null;
        commPortIdentifier = null;
    }

    public boolean isPortOpen() {
        return isPortOpen;
    }

}
