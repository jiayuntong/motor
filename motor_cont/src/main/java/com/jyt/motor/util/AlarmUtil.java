package com.jyt.motor.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.Semaphore;

public class AlarmUtil {
    static Log log = LogFactory.getLog(AlarmUtil.class);
    static AlarmClass ac = new AlarmClass();
    static Semaphore semaphore = new Semaphore(1);

    /**
     * 异步多线程打开串口报警灯
     *
     * @param alarmSerialPort
     */
    public static void openAlarmLamp(String alarmSerialPort) {
        try {
            semaphore.acquire();
            ac.selectPort(alarmSerialPort);
            byte[] message = {(byte) 0x01, (byte) 0x05, 0x00, 0x00, (byte) 0xFF, 0x00, (byte) 0x8C, (byte) 0x3A};
            ac.write(message);
            ac.close();
            semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 异步多线程关闭串口报警灯
     *
     * @param alarmSerialPort
     */
    public static void closeAlarmLamp(String alarmSerialPort) {
        try {
            semaphore.acquire();
            ac.selectPort(alarmSerialPort);
            byte[] message = {(byte) 0x01, (byte) 0x05, 0x00, 0x00, 0x00, 0x00, (byte) 0xCD, (byte) 0xCA};
            ac.write(message);
            ac.close();
            semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}