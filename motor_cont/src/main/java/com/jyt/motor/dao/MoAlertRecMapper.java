package com.jyt.motor.dao;

import com.jyt.motor.model.MoAlertRec;
import com.jyt.motor.vo.CallParaMotorsExcept;
import com.jyt.motor.vo.DashboardAlertInfo;
import com.jyt.motor.vo.MoAlertRecView;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface MoAlertRecMapper {

    int insert(MoAlertRec record);

    int insertSelective(MoAlertRec record);

    int updateByPrimaryKey(MoAlertRec record);

    int updateByPrimaryKeySelective(MoAlertRec record);

    int deleteByPrimaryKey(java.lang.Integer id);

    MoAlertRec selectByPrimaryKey(java.lang.Integer imId);

    List<MoAlertRec> selectAll();

    int insertBatch(List<MoAlertRec> list);

    List<MoAlertRecView> moAlertRecSelect(CallParaMotorsExcept parameter);

    Long moAlertRecCount(CallParaMotorsExcept parameter);

    List<DashboardAlertInfo> moAlertInfoDashboard(@Param("periods") Integer periods);

    List<MoAlertRec> selectUntreated(Integer moId);

    int updateRecoveryByIds(@Param("dealTime") Date dealTime, @Param("dealContent") String dealContent, @Param("alertClass") Integer alertClass, @Param("list") List<Integer> list);

    int updateRecoveryByRecs(@Param("dealTime") Date dealTime, @Param("dealContent") String dealContent, @Param("list") List<MoAlertRec> list);

    int updateRecoveryByMoIds(@Param("dealTime") Date dealTime, @Param("dealContent") String dealContent, @Param("list") List<Integer> list);

    int updateDealing(@Param("dealContent") String dealContent);

}
