package com.jyt.motor.dao;

import com.jyt.motor.model.AuthMod;
import com.jyt.motor.model.SysUsers;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface SysUsersMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(SysUsers record);

    int insertSelective(SysUsers record);

    SysUsers selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(SysUsers record);

    int updateByPrimaryKey(SysUsers record);

    SysUsers getUser(@Param("userName") String userName);

    List<SysUsers> getAllUser();

    int setChgPwd(SysUsers users);

    int setChgPwdByUserId(SysUsers users);

    List<AuthMod> getAuth(int userId);

    List<SysUsers> getAll();

    List<String> getOperation(int roleId);

    List<SysUsers> getAllFlowMeterUsers(int roleId);

}
