package com.jyt.motor.dao;

import java.util.List;
import com.jyt.motor.model.SysConfig;
import org.apache.ibatis.annotations.Param;

public interface SysConfigMapper {

    int insert(SysConfig record);

    int insertSelective(SysConfig record);

    int updateByPrimaryKey(SysConfig record);

    int updateByPrimaryKeySelective(SysConfig record);

    int deleteByPrimaryKey(java.lang.Integer id);

    SysConfig selectByPrimaryKey(java.lang.Integer id);

    List<SysConfig> selectAll();

    SysConfig selectByConfKey(@Param("confKey") String confKey);

}
