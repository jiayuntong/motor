package com.jyt.motor.dao;

import com.jyt.motor.model.MoDataRec;
import com.jyt.motor.vo.CallParMotorsReport;
import com.jyt.motor.vo.DashboardRecentInfo;
import com.jyt.motor.vo.DataAvgVo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface MoDataRecMapper {

    int insert(MoDataRec record);

    int insertSelective(MoDataRec record);

    int updateByPrimaryKey(MoDataRec record);

    int updateByPrimaryKeySelective(MoDataRec record);

    int deleteByPrimaryKey(java.lang.Long id);

    MoDataRec selectByPrimaryKey(java.lang.Long recId);

    List<MoDataRec> selectAll();

    int insertBatch(List<MoDataRec> list);

    List<MoDataRec> selectDataRec(@Param("list") List<Integer> list);

    List<MoDataRec> moDataRecSelect(CallParMotorsReport parameter);

    Integer moDataRecCount(CallParMotorsReport parameter);

    BigDecimal getMoRockDataAvg(@Param("moId") Integer moId, @Param("fromTime") Date fromTime, @Param("toTime") Date toTime);

    DataAvgVo getMoRockDataAvgVo(@Param("moId") Integer moId, @Param("fromTime") Date fromTime, @Param("toTime") Date toTime);

    List<DashboardRecentInfo> selectRecent(@Param("moId") Integer moId);

    Date getEarliestRecTime(@Param("moId") Integer moId);
}