package com.jyt.motor.dao;

import com.jyt.motor.model.MoDataReport;
import com.jyt.motor.vo.DataAvgVo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface MoDataReportMapper {

    int insert(MoDataReport record);

    int insertSelective(MoDataReport record);

    int updateByPrimaryKey(MoDataReport record);

    int updateByPrimaryKeySelective(MoDataReport record);

    int deleteByPrimaryKey(java.lang.Long id);

    MoDataReport selectByPrimaryKey(java.lang.Long imId);

    List<MoDataReport> selectAll();

    List<MoDataReport> selectByCPChat(@Param("repType") Integer repType,
                                      @Param("listMoIds") List<Integer> listMoIds,
                                      @Param("startTime") String startTime,
                                      @Param("endTime") String endTime);

    Long existModelByLogicKey(MoDataReport record);

    Long getEarliestByMoId(@Param("type") Integer type, @Param("moId") Integer moId);
    //MoDataReport selectByCondition(Integer type, Integer moId, String timeArea);

    BigDecimal getMoHourDataAvg(@Param("moId") Integer moId, @Param("fromDate") String fromDate, @Param("toDate") String toDate);

    DataAvgVo getMoDataAvgVo(@Param("moId") Integer moId, @Param("type") Integer type, @Param("fromDate") String fromDate, @Param("toDate") String toDate);
}