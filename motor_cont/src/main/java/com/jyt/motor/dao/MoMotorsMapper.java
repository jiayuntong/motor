package com.jyt.motor.dao;

import com.jyt.motor.model.MoMotors;
import com.jyt.motor.model.MotorMonitor;
import com.jyt.motor.vo.MotorLatestRepIdView;
import com.jyt.motor.vo.MotorView;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MoMotorsMapper {
    MoMotors selectByPrimaryKey(Integer moId);

    int deleteByPrimaryKey(Integer moId);

    int insertSelective(MoMotors record);

    int updateByPrimaryKeySelective(MoMotors record);

    int deleteFake(Integer moId);

    List<MoMotors> motorsSelect();

    List<MotorView> motorViewsSelect();

    List<MotorMonitor> motorMoniSelect(@Param(value = "addr") String addr, @Param("type") Integer type);

    int updateStateByIds(@Param("state") Integer state, @Param("moLight") String moLight,@Param("list") List<Integer> list);

    List<MoMotors> selectMortor(@Param("areaId") Integer areaId);

    List<MoMotors> selectMortorByPeriods(@Param("periods") Integer periods);

    List<Integer> selectMoIdsByMoState(@Param("moState") Integer moState);

    List<MotorLatestRepIdView> selectAllLatestRepId();

    MotorLatestRepIdView getLatestRepIdById(Integer motorId);

    int updateLatestRepIdById(MotorLatestRepIdView mod);

    int countAlarmMotor();
}