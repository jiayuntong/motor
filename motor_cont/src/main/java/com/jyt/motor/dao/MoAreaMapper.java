package com.jyt.motor.dao;

import java.util.List;
import com.jyt.motor.model.MoArea;

public interface MoAreaMapper {

    int insert(MoArea record);

    int insertSelective(MoArea record);

    int updateByPrimaryKey(MoArea record);

    int updateByPrimaryKeySelective(MoArea record);

    int deleteByPrimaryKey(java.lang.Integer id);

    MoArea selectByPrimaryKey(java.lang.Integer imId);

    List<MoArea> selectAll();

}
