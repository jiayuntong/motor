package com.jyt.motor.dao;

import com.jyt.motor.model.MoConverter;
import com.jyt.motor.model.MoMotors;
import com.jyt.motor.vo.MotorView;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MoConverterMapper {
    int insert(MoConverter record);

    int insertSelective(MoConverter record);

    int updateByPrimaryKey(MoConverter record);

    int updateByPrimaryKeySelective(MoConverter record);

    int deleteByPrimaryKey(java.lang.Integer id);

    MoConverter selectByPrimaryKey(java.lang.Integer imId);

    List<MoConverter> selectAll();

    List<MoConverter> getInUseConverter(@Param("converterType") int converterType);


}