package com.jyt.motor.dao;

import java.util.List;
import com.jyt.motor.model.MoMonitors;
import com.jyt.motor.vo.MoMonitorsAndConverter;
import com.jyt.motor.vo.MonitorView;

public interface MoMonitorsMapper {

    int insert(MoMonitors record);

    int insertSelective(MoMonitors record);

    int updateByPrimaryKey(MoMonitors record);

    int updateByPrimaryKeySelective(MoMonitors record);

    int deleteByPrimaryKey(java.lang.Integer id);

    MoMonitors selectByPrimaryKey(java.lang.Integer imId);

    List<MoMonitors> selectAll();

    List<MoMonitorsAndConverter> motorsSelect2();

    List<MonitorView> canInstalled();

}
