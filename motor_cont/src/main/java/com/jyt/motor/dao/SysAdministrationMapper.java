package com.jyt.motor.dao;

import com.jyt.motor.model.SysMenus;
import com.jyt.motor.model.SysRoles;
import com.jyt.motor.model.SysUsers;
import com.jyt.motor.vo.SysRolesVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysAdministrationMapper {
    List<SysMenus> getMeun();

    Integer updMeun(SysMenus menus);

    Integer addMeun(SysMenus menus);

    Integer delMenu(Integer menuId);

    List<SysRoles> getRoles();

    Integer addRoles(SysRoles roles);

    Integer updRoles(SysRoles roles);

    Integer delRoles(int roleId);

    Integer rolesToMeun(@Param("roleId") Integer roleId, @Param("menuId") String[] menuId);

    List<Integer> exceptMenusOfRole(@Param("roleId")Integer roleId , @Param("menuIds") String[] menuIds);

    Integer delRoleMenu(@Param("roleId") Integer roleId, @Param("menuIds") Integer[] menuIds);

    List<SysUsers> getUsers();

    List<SysRolesVo> getRoleIdToMeun(List<SysRoles> roleId);

}
