package com.jyt.motor.model;

public class SysRoleOperation {
    private Integer sn;
    private Integer roleId;
    private String operation;

    public Integer getSn() {
        return this.sn;
    }
    public void setSn(Integer sn) {
        this.sn = sn;
    }

    public Integer getRoleId() {
        return this.roleId;
    }
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getOperation() {
        return this.operation;
    }
    public void setOperation(String operation) {
        this.operation = operation;
    }
}