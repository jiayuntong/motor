package com.jyt.motor.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysConfig {
    private Integer id;

    private String confKey;

    private String confValue;

    private String confDesc;

    private String confCategory;

    @Override
    public String toString() {
        return "SysConfig{" +
                "id=" + id +
                ", confKey='" + confKey + '\'' +
                ", confValue='" + confValue + '\'' +
                ", confDesc='" + confDesc + '\'' +
                ", confCategory='" + confCategory + '\'' +
                '}';
    }
}
