package com.jyt.motor.model;

public class MoArea {
    private Integer areaId;
    private String areaName;
    private Integer sortSn;
    private Integer levelNo;
    private Integer areaPId;

    public Integer getAreaId() {
        return this.areaId;
    }
    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return this.areaName;
    }
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Integer getSortSn() {
        return this.sortSn;
    }
    public void setSortSn(Integer sortSn) {
        this.sortSn = sortSn;
    }

    public Integer getLevelNo() {
        return this.levelNo;
    }
    public void setLevelNo(Integer levelNo) {
        this.levelNo = levelNo;
    }

    public Integer getAreaPId() {
        return this.areaPId;
    }
    public void setAreaPId(Integer areaPId) {
        this.areaPId = areaPId;
    }
}