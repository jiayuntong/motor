package com.jyt.motor.model;

import com.jyt.motor.constant.context.ThreadStore;

import javax.swing.text.AbstractDocument;
import java.util.Date;

/**
 * @program: motor_cont
 * @description:
 * @author: guojian huang
 * @create: 2020-08-21 15:16
 */
public class MoNotes {
    private Integer msgId;
    private String groupId;
    private String userName;
    private String mobiles;
    private String content;
    private Date submitTime;
    private Date sendTime;
    private int status;
    private String type;

    public MoNotes(String mobiles, String content) {
        super();
        this.mobiles = mobiles;
        this.content = content;
    }

    public Integer getMsgId() {
        return msgId;
    }

    public void setMsgId(Integer msgId) {
        this.msgId = msgId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobiles() {
        return mobiles;
    }

    public void setMobiles(String mobiles) {
        this.mobiles = mobiles;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
