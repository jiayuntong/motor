package com.jyt.motor.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthMod {
    private int menuId;
    private String userName;
    private String nickName;
    private String roleName;
    private String menuName;
    private String menuCode;
    private String menuSort;
    private String menuTitle;
    private String menuUrl;
    private int pMenuId;
    private String ioce;
    private int sysMenus;
    private List<AuthMod> lists;


    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public String getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }


    public String getMenuSort() {
        return menuSort;
    }

    public void setMenuSort(String menuSort) {
        this.menuSort = menuSort;
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public int getpMenuId() {
        return pMenuId;
    }

    public void setpMenuId(int pMenuId) {
        this.pMenuId = pMenuId;
    }

    public List<AuthMod> getLists() {
        return lists;
    }

    public void setLists(List<AuthMod> lists) {
        this.lists = lists;
    }

    public String getIoce() {
        return ioce;
    }

    public void setIoce(String ioce) {
        this.ioce = ioce;
    }

    public int getSysMenus() {
        return sysMenus;
    }

    public void setSysMenus(int sysMenus) {
        this.sysMenus = sysMenus;
    }

    @Override
    public String toString() {
        return "AuthMod{" +
                "menuId=" + menuId +
                ", userName='" + userName + '\'' +
                ", nickName='" + nickName + '\'' +
                ", roleName='" + roleName + '\'' +
                ", menuName='" + menuName + '\'' +
                ", menuCode='" + menuCode + '\'' +
                ", menuSort='" + menuSort + '\'' +
                ", menuTitle='" + menuTitle + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", pMenuId=" + pMenuId +
                ", ioce='" + ioce + '\'' +
                ", sysMenus=" + sysMenus +
                ", lists=" + lists +
                '}';
    }
}
