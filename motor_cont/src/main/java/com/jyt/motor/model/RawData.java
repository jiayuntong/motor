package com.jyt.motor.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RawData {
    private String addr;

    private Date recTime;

    private List<Integer> data;

    public RawData(String addr,int data1,int data2,Date recTime)
    {
        this.addr=addr;
        data=new ArrayList<>();
        data.add(data1);
        data.add(data2);
        this.recTime=recTime;
    }
    public RawData(String addr,int data1,int data2,int data3,int data4,int data5,int data6,int data7,int data8,Date recTime)
    {
        this.addr=addr;
        data=new ArrayList<>();
        data.add(data1);
        data.add(data2);
        data.add(data3);
        data.add(data4);
        data.add(data5);
        data.add(data6);
        data.add(data7);
        data.add(data8);
        this.recTime=recTime;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public List<Integer> getData() {
        return data;
    }

    public void setData(List<Integer> data) {
        this.data = data;
    }

    public Date getRecTime() {
        return recTime;
    }

    public void setRecTime(Date recTime) {
        this.recTime = recTime;
    }
}
