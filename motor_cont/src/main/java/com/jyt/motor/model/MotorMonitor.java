package com.jyt.motor.model;

import java.math.BigDecimal;

public class MotorMonitor extends  MoMotors{

    private Integer bitSn;
    private BigDecimal fixBase;
    private Integer inputFrom;
    private Integer inputTo;
    private Integer valueFrom;
    private Integer valueTo;
    private String areaName;

    public Integer getBitSn() {
        return bitSn;
    }

    public void setBitSn(Integer bitSn) {
        this.bitSn = bitSn;
    }

    public BigDecimal getFixBase() {
        return fixBase;
    }

    public void setFixBase(BigDecimal fixBase) {
        this.fixBase = fixBase;
    }

    public Integer getInputFrom() {
        return inputFrom;
    }

    public void setInputFrom(Integer inputFrom) {
        this.inputFrom = inputFrom;
    }

    public Integer getInputTo() {
        return inputTo;
    }

    public void setInputTo(Integer inputTo) {
        this.inputTo = inputTo;
    }

    public Integer getValueFrom() {
        return valueFrom;
    }

    public void setValueFrom(Integer valueFrom) {
        this.valueFrom = valueFrom;
    }

    public Integer getValueTo() {
        return valueTo;
    }

    public void setValueTo(Integer valueTo) {
        this.valueTo = valueTo;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }
}