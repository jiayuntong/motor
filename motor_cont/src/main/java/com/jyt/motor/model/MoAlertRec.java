package com.jyt.motor.model;

import com.jyt.motor.constant.context.ThreadStore;

import java.util.Date;
import java.math.BigDecimal;

public class MoAlertRec {
    private Integer id;
    private Integer recId;
    private Integer alertClass;
    private Integer moId;
    private String moName;
    private Integer areaId;
    private String areaName;
    private Date alertTime;
    private BigDecimal rockData;
    private BigDecimal upperLimit;
    private BigDecimal lowerLimit;
    private Integer hasDealed;
    private Date dealTime;
    private String dealContent;
    private String alertMsg;

    public MoAlertRec()
    {
        super();
    }

    public MoAlertRec(Integer alertClass, Integer moId, String moName, Integer areaId, String areaName, Date alertTime, BigDecimal rockData, BigDecimal upperLimit, BigDecimal lowerLimit, Integer hasDealed, String alertMsg) {
        this.alertClass=alertClass;
        this.moId=moId;
        this.moName=moName;
        this.areaId=areaId;
        this.areaName=areaName;
        this.alertTime=alertTime;
        this.rockData=rockData;
        this.upperLimit=upperLimit;
        this.lowerLimit=lowerLimit;
        this.hasDealed=hasDealed;
        this.alertMsg=alertMsg;
    }

    public Integer getId() {
        return this.id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRecId() {
        return this.recId;
    }
    public void setRecId(Integer recId) {
        this.recId = recId;
    }

    public Integer getAlertClass() {
        return this.alertClass;
    }
    public void setAlertClass(Integer alertClass) {
        this.alertClass = alertClass;
    }

    public Integer getMoId() {
        return this.moId;
    }
    public void setMoId(Integer moId) {
        this.moId = moId;
    }

    public String getMoName() {
        return this.moName;
    }
    public void setMoName(String moName) {
        this.moName = moName;
    }

    public Integer getAreaId() {
        return this.areaId;
    }
    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return this.areaName;
    }
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Date getAlertTime() {
        return this.alertTime;
    }
    public void setAlertTime(Date alertTime) {
        this.alertTime = alertTime;
    }

    public BigDecimal getRockData() {
        return this.rockData;
    }
    public void setRockData(BigDecimal rockData) {
        this.rockData = rockData;
    }

    public BigDecimal getUpperLimit() {
        return this.upperLimit;
    }
    public void setUpperLimit(BigDecimal upperLimit) {
        this.upperLimit = upperLimit;
    }

    public BigDecimal getLowerLimit() {
        return this.lowerLimit;
    }
    public void setLowerLimit(BigDecimal lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public Integer getHasDealed() {
        return this.hasDealed;
    }
    public void setHasDealed(Integer hasDealed) {
        this.hasDealed = hasDealed;
    }

    public Date getDealTime() {
        return this.dealTime;
    }
    public void setDealTime(Date dealTime) {
        this.dealTime = dealTime;
    }

    public String getDealContent() {
        return this.dealContent;
    }
    public void setDealContent(String dealContent) {
        this.dealContent = dealContent;
    }

    public String getAlertMsg() {
        return this.alertMsg;
    }
    public void setAlertMsg(String alertMsg) {
        this.alertMsg = alertMsg;
    }
}