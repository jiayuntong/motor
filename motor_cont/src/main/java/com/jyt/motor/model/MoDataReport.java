package com.jyt.motor.model;

import java.math.BigDecimal;
import java.util.Date;

public class MoDataReport {
    private Long id;
    private Integer type;
    private Integer moId;
    private Date time;
    private String timeArea;
    private BigDecimal data;
    private BigDecimal upperLimit;
    private BigDecimal lowerLimit;
    private float maintainRate;
    private float failureRate;
    private float dynamicRate;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getMoId() {
        return this.moId;
    }

    public void setMoId(Integer moId) {
        this.moId = moId;
    }

    public Date getTime() {
        return this.time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getTimeArea() {
        return this.timeArea;
    }

    public void setTimeArea(String timeArea) {
        this.timeArea = timeArea;
    }

    public BigDecimal getData() {
        return this.data;
    }

    public void setData(BigDecimal data) {
        this.data = data;
    }

    public BigDecimal getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(BigDecimal upperLimit) {
        this.upperLimit = upperLimit;
    }

    public BigDecimal getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(BigDecimal lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public float getMaintainRate() {
        return maintainRate;
    }

    public void setMaintainRate(float maintainRate) {
        this.maintainRate = maintainRate;
    }

    public float getFailureRate() {
        return failureRate;
    }

    public void setFailureRate(float failureRate) {
        this.failureRate = failureRate;
    }

    public float getDynamicRate() {
        return dynamicRate;
    }

    public void setDynamicRate(float dynamicRate) {
        this.dynamicRate = dynamicRate;
    }
}