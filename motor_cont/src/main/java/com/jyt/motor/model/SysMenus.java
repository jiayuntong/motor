package com.jyt.motor.model;

public class SysMenus {
    private Integer menuId;

    private String menuName;

    private Integer menuSort;

    private String menuCode;

    private String menuTitle;

    private String menuUrl;

    private Integer pMenuId;

    private String ioce;

    private Integer sysMenus;

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName == null ? null : menuName.trim();
    }

    public Integer getMenuSort() {
        return menuSort;
    }

    public void setMenuSort(Integer menuSort) {
        this.menuSort = menuSort;
    }

    public String getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode == null ? null : menuCode.trim();
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle == null ? null : menuTitle.trim();
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl == null ? null : menuUrl.trim();
    }

    public Integer getpMenuId() {
        return pMenuId;
    }

    public void setpMenuId(Integer pMenuId) {
        this.pMenuId = pMenuId;
    }

    public String getIoce() {
        return ioce;
    }

    public void setIoce(String ioce) {
        this.ioce = ioce;
    }

    public Integer getSysMenus() {
        return sysMenus;
    }

    public void setSysMenus(Integer sysMenus) {
        this.sysMenus = sysMenus;
    }

    @Override
    public String toString() {
        return "SysMenus{" +
                "menuId=" + menuId +
                ", menuName='" + menuName + '\'' +
                ", menuSort=" + menuSort +
                ", menuCode='" + menuCode + '\'' +
                ", menuTitle='" + menuTitle + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", pMenuId=" + pMenuId +
                ", ioce='" + ioce + '\'' +
                ", sysMenus=" + sysMenus +
                '}';
    }
}
