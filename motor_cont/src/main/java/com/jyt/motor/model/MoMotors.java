package com.jyt.motor.model;

import java.math.BigDecimal;

public class MoMotors {
    private Integer moId;

    private String moName;

    private Integer areaId;

    private Integer useFlag;

    private BigDecimal upperLimit;

    private BigDecimal lowerLimit;

    private BigDecimal shockThreshold;

    private Integer monitorId;

    private Integer moState;

    private String moLight;

    private BigDecimal standardValue;

    private Integer periods;

    private Integer canAlertLight;

    private Integer latestHourlyId;
    private Integer latestDailyId;
    private Integer latestMonthlyId;

    private BigDecimal upperLimitInner;
    private BigDecimal lowerLimitInner;

    private String notes;

    public Integer getMoId() {
        return moId;
    }

    public void setMoId(Integer moId) {
        this.moId = moId;
    }

    public String getMoName() {
        return moName;
    }

    public void setMoName(String moName) {
        this.moName = moName;
    }

    public Integer getUseFlag() {
        return useFlag;
    }

    public void setUseFlag(Integer useFlag) {
        this.useFlag = useFlag;
    }

    public BigDecimal getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(BigDecimal upperLimit) {
        this.upperLimit = upperLimit;
    }

    public BigDecimal getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(BigDecimal lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public BigDecimal getShockThreshold() {
        return shockThreshold;
    }

    public void setShockThreshold(BigDecimal shockThreshold) {
        this.shockThreshold = shockThreshold;
    }

    public Integer getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Integer monitorId) {
        this.monitorId = monitorId;
    }

    /**
     * @return 马达状态(0.离线, 1.正常, 2.异常, 3.保养中)
     */
    public Integer getMoState() {
        return moState;
    }

    public void setMoState(Integer moState) {
        this.moState = moState;
    }

    public String getMoLight() {
        return moLight;
    }

    public void setMoLight(String moLight) {
        this.moLight = moLight;
    }

    public BigDecimal getStandardValue() {
        return standardValue;
    }

    public void setStandardValue(BigDecimal standardValue) {
        this.standardValue = standardValue;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getPeriods() {
        return this.periods;
    }

    public void setPeriods(Integer periods) {
        this.periods = periods;
    }

    public Integer getCanAlertLight() {
        return canAlertLight;
    }

    public void setCanAlertLight(Integer canAlertLight) {
        this.canAlertLight = canAlertLight;
    }

    public Integer getLatestHourlyId() {
        return latestHourlyId;
    }

    public void setLatestHourlyId(Integer latestHourlyId) {
        this.latestHourlyId = latestHourlyId;
    }

    public Integer getLatestDailyId() {
        return latestDailyId;
    }

    public void setLatestDailyId(Integer latestDailyId) {
        this.latestDailyId = latestDailyId;
    }

    public Integer getLatestMonthlyId() {
        return latestMonthlyId;
    }

    public void setLatestMonthlyId(Integer latestMonthlyId) {
        this.latestMonthlyId = latestMonthlyId;
    }

    public BigDecimal getUpperLimitInner() {
        return upperLimitInner;
    }

    public void setUpperLimitInner(BigDecimal upperLimitInner) {
        this.upperLimitInner = upperLimitInner;
    }

    public BigDecimal getLowerLimitInner() {
        return lowerLimitInner;
    }

    public void setLowerLimitInner(BigDecimal lowerLimitInner) {
        this.lowerLimitInner = lowerLimitInner;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}