package com.jyt.motor.model;

import java.util.Date;
import java.math.BigDecimal;

public class MoMonitors {
    private Integer monitorId;
    private Integer gauge;
    private Integer valueFrom;
    private Integer valueTo;
    private Integer converterId;
    private Integer bitSn;
    private Date installTime;
    private BigDecimal fixBase;
    private Integer inputFrom;
    private Integer inputTo;
    private Integer monitorState;
    private String monitorLight;

    public Integer getMonitorId() {
        return this.monitorId;
    }
    public void setMonitorId(Integer monitorId) {
        this.monitorId = monitorId;
    }

    public Integer getGauge() {
        return this.gauge;
    }
    public void setGauge(Integer gauge) {
        this.gauge = gauge;
    }

    public Integer getValueFrom() {
        return this.valueFrom;
    }
    public void setValueFrom(Integer valueFrom) {
        this.valueFrom = valueFrom;
    }

    public Integer getValueTo() {
        return this.valueTo;
    }
    public void setValueTo(Integer valueTo) {
        this.valueTo = valueTo;
    }

    public Integer getConverterId() {
        return this.converterId;
    }
    public void setConverterId(Integer converterId) {
        this.converterId = converterId;
    }

    public Integer getBitSn() {
        return this.bitSn;
    }
    public void setBitSn(Integer bitSn) {
        this.bitSn = bitSn;
    }

    public Date getInstallTime() {
        return this.installTime;
    }
    public void setInstallTime(Date installTime) {
        this.installTime = installTime;
    }

    public BigDecimal getFixBase() {
        return this.fixBase;
    }
    public void setFixBase(BigDecimal fixBase) {
        this.fixBase = fixBase;
    }

    public Integer getInputFrom() {
        return this.inputFrom;
    }
    public void setInputFrom(Integer inputFrom) {
        this.inputFrom = inputFrom;
    }

    public Integer getInputTo() {
        return this.inputTo;
    }
    public void setInputTo(Integer inputTo) {
        this.inputTo = inputTo;
    }

    public Integer getMonitorState() {
        return this.monitorState;
    }
    public void setMonitorState(Integer monitorState) {
        this.monitorState = monitorState;
    }

    public String getMonitorLight() {
        return this.monitorLight;
    }
    public void setMonitorLight(String monitorLight) {
        this.monitorLight = monitorLight;
    }
}