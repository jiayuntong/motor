package com.jyt.motor.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysRoles {
    private Integer roleId;

    private String roleName;

    private String roleRemark;


    @Override
    public String toString() {
        return "SysRoles{" +
                "roleId=" + roleId +
                ", roleName='" + roleName + '\'' +
                ", roleRemark='" + roleRemark + '\'' +
                '}';
    }
}
