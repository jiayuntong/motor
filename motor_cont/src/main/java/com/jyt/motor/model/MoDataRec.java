package com.jyt.motor.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.alibaba.fastjson.JSON;

public class MoDataRec {

    private Integer recId;
    private Integer moId;
    private String moName;
    private Integer areaId;
    private String areaName;
    private Integer monitorId;
    private Date recTime;
    private BigDecimal origData;
    private BigDecimal rockData;
    private BigDecimal upperLimit;
    private BigDecimal lowerLimit;
    private Integer state;
    private BigDecimal fixBase;
    private BigDecimal displacementData;
    private int isOver;

    public MoDataRec()
    {
        super();
    }

    public MoDataRec(Integer moId, String moName, Integer areaId, String areaName, Integer monitorId, Date recTime, BigDecimal upperLimit, BigDecimal lowerLimit, Integer state, BigDecimal fixBase) {
        this.moId = moId;
        this.moName = moName;
        this.areaId = areaId;
        this.areaName = areaName;
        this.monitorId = monitorId;
        this.recTime = recTime;
        this.upperLimit = upperLimit;
        this.lowerLimit = lowerLimit;
        this.state = state;
        this.fixBase = fixBase;
    }

    public Integer getRecId() {
        return recId;
    }

    public void setRecId(Integer recId) {
        this.recId = recId;
    }

    public Integer getMoId() {
        return moId;
    }

    public void setMoId(Integer moId) {
        this.moId = moId;
    }

    public String getMoName() {
        return moName;
    }

    public void setMoName(String moName) {
        this.moName = moName;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Integer getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Integer monitorId) {
        this.monitorId = monitorId;
    }

    public Date getRecTime() {
        return recTime;
    }

    public void setRecTime(Date recTime) {
        this.recTime = recTime;
    }

    public BigDecimal getOrigData() {
        return origData;
    }

    public void setOrigData(BigDecimal origData) {
        this.origData = origData;
    }

    public BigDecimal getRockData() {
        return rockData;
    }

    public void setRockData(BigDecimal rockData) {
        this.rockData = rockData;
    }

    public BigDecimal getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(BigDecimal upperLimit) {
        this.upperLimit = upperLimit;
    }

    public BigDecimal getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(BigDecimal lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public BigDecimal getFixBase() {
        return fixBase;
    }

    public void setFixBase(BigDecimal fixBase) {
        this.fixBase = fixBase;
    }

    public BigDecimal getDisplacementData() {
        return displacementData;
    }

    public void setDisplacementData(BigDecimal displacementData) {
        this.displacementData = displacementData;
    }

    public int getIsOver() {
        return isOver;
    }

    public void setIsOver(int isOver) {
        this.isOver = isOver;
    }
}
