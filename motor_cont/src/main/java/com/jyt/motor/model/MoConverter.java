package com.jyt.motor.model;

public class MoConverter {
    private Integer converterId;

    private String converterAddr;

    private Integer converterType;

    private Integer inUse;

    private String command;

    public Integer getConverterId() {
        return converterId;
    }

    public void setConverterId(Integer converterId) {
        this.converterId = converterId;
    }

    public String getConverterAddr() {
        return converterAddr;
    }

    public void setConverterAddr(String converterAddr) {
        this.converterAddr = converterAddr == null ? null : converterAddr.trim();
    }

    public Integer getInUse() {
        return inUse;
    }

    public void setInUse(Integer inUse) {
        this.inUse = inUse;
    }

    public Integer getConverterType() {
        return converterType;
    }

    public void setConverterType(Integer converterType) {
        this.converterType = converterType;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}