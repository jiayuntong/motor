package com.jyt.motor.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysRoleMenu {
    private Integer menuId;

    private Integer roleId;


}
